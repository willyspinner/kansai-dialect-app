package wilson_j.kansaiben_project;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;
import wilson_j.kansaiben_project.data.structures.SentenceSuggestionFactory;
import wilson_j.kansaiben_project.data.structures.Suggestion;


public class Stage2BActivity extends AppCompatActivity {
    SharedPreferences sharedPreferences;

    SentenceSuggestionFactory ssf;
    ProgressBar progressBar;
    RatingBar ratingBar;
    Button b1;
    Button b2;
    Button b3;
    Button b4;
    Button b5;
    Button b6;
    Button sentenceHint;
    Button zeroRateButton;
    Button exitButton;
    Button kansaiAnswerButton;
    Button endingHint;
    TextView timeRemaining;
    TextView scoreTextView;


    //we have a currentSentenceEnding.
    Character[] currentSentenceEnding;
    Suggestion currentSuggestion;


    String mySentenceString;
    int indexCount;

    Vibrator vibrator;
    int no_done;
 //   int total;
    int streak;
    long time;
int questions;
    long score;
    CountDownTimer countDownTimer;
    Runnable task;

    //for stage 3 purposes,:
    ArrayList<Integer>stage3Sequences;
    Intent fromIntent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
       // veryBad=false;

        ssf= new SentenceSuggestionFactory(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stage2_b);
       sharedPreferences = getSharedPreferences("sharedPreferences",MODE_PRIVATE);
        ratingBar = (RatingBar)findViewById(R.id.ratingBar_stage_2b_ratingBar);
        vibrator = (Vibrator)getSystemService(VIBRATOR_SERVICE);
        progressBar= (ProgressBar)findViewById(R.id.progressBar_stage_2b_progress);
        fromIntent = getIntent();
         stage3Sequences = fromIntent.getIntegerArrayListExtra("stage3Sequences");
        questions=stage3Sequences==null?8:stage3Sequences.remove(0);
        progressBar.setMax(questions);//8 questions default.
        progressBar.setProgress(0);
        b1 = (Button)findViewById(R.id.button_stage_2b_ans1);
        b2 = (Button)findViewById(R.id.button_stage_2b_ans2);
        b3 = (Button)findViewById(R.id.button_stage_2b_ans3);
        b4 = (Button)findViewById(R.id.button_stage_2b_ans4);
        b5 = (Button)findViewById(R.id.button_stage_2b_ans5);
        b6 = (Button)findViewById(R.id.button_stage_2b_ans6);

      zeroRateButton = (Button)findViewById(R.id.button_stage_2b_wierdSentence);
        exitButton = (Button)findViewById(R.id.button_stage_2b_exit);
        kansaiAnswerButton = (Button)findViewById(R.id.button_stage_2b_kansaiAnswer);
        sentenceHint = (Button)findViewById(R.id.button_stage_2b_fullSentence);
        endingHint = (Button)findViewById(R.id.button_stage_2b_japaneseHint);
        timeRemaining = (TextView)findViewById(R.id.textView_stage_2b_timeRemaining);
        scoreTextView= (TextView)findViewById(R.id.textView_stage_2b_score);
        //below is for determining whether this was launched from stage 3 or from main menu.


exitButton.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        countDownTimer.cancel();
Intent intent = new Intent(Stage2BActivity.this,MainActivity.class);
        ssf.close();
        startActivity(intent);
    }
});



        //manage event handling for answer submission below:

        kansaiAnswerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                indexCount=0;

               ssf.setRating(currentSuggestion, (short)ratingBar.getProgress());
                    //^^int to short typecasting is ok since the ratingbar's progress ranges from 0 to 5 anyway.

                if(mySentenceString.equals(currentSuggestion.getSentenceString(Suggestion.MODE_KANSAI))){
                    //correct
                    question_answered_correctly();

                }else{
                    question_answered_incorrectly();
                }

          //  mySentenceString="";
            }
        });
        kansaiAnswerButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

               indexCount=0;

                mySentenceString =currentSuggestion.getSentenceArray(Suggestion.MODE_KANSAI)[0]+currentSuggestion.getSentenceArray(Suggestion.MODE_KANSAI)[1]+currentSuggestion.getSentenceArray(Suggestion.MODE_KANSAI)[2] ;

                kansaiAnswerButton.setText(mySentenceString);
                fill_up_answer_choices();

                return true;
            }
        });

        //for the 6 buttons, event handling below:
        View.OnClickListener answerChoiceListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Button clicked=(Button)findViewById(v.getId());

                mySentenceString=mySentenceString+(clicked.getText().toString());//the clicked button's text is just a single character.
                indexCount++;



                if(indexCount>= currentSentenceEnding.length){
                    b1.setText("");
                    b2.setText("");
                    b3.setText("");
                    b4.setText("");
                    b5.setText("");
                    b6.setText("");
                    kansaiAnswerButton.setText(mySentenceString);
                }else {
                    kansaiAnswerButton.setText(mySentenceString);
                    fill_up_answer_choices();
                }
            }
        };
        b1.setOnClickListener(answerChoiceListener);
        b2.setOnClickListener(answerChoiceListener);
        b3.setOnClickListener(answerChoiceListener);
        b4.setOnClickListener(answerChoiceListener);
        b5.setOnClickListener(answerChoiceListener);
        b6.setOnClickListener(answerChoiceListener);

        zeroRateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {





ssf.setRating(currentSuggestion,(short)0);
                generateNewAnswer();
                fill_up_answer_choices();
                ratingBar.setProgress(5);
                updateTimer();

        }});




        kansaiAnswerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //set the rating to the SSF.

                ssf.setRating(currentSuggestion,(short)ratingBar.getProgress());
                //convert the currentSentenceEnding from a char array to a string.


                if(mySentenceString.equals(currentSuggestion.getSentenceString(Suggestion.MODE_KANSAI))){
                    question_answered_correctly();

                }else{question_answered_incorrectly();}


            }
        });
//initialise all data variables.

        ratingBar.setMax(5);
        ratingBar.setProgress(5);


        no_done=0;
        score=0;
        generateNewAnswer();
        fill_up_answer_choices();
        mySentenceString =currentSuggestion.getSentenceArray(Suggestion.MODE_KANSAI)[0]+currentSuggestion.getSentenceArray(Suggestion.MODE_KANSAI)[1]+currentSuggestion.getSentenceArray(Suggestion.MODE_KANSAI)[2] ;

        updateTimer();
    }

//questions when answered right or wrong, make an invocation to the two methods below.
    private void question_answered_correctly(){


        streak++;
        no_done++;
        score += (long) Math.ceil(((Double.parseDouble(timeRemaining.getText().toString())*1000)/(double)time)
                *(10*(streak+1)));
        scoreTextView.setText(Long.toString(score));

         getWindow().getDecorView().setBackgroundColor(getResources().getColor(R.color.right_answer));
         task = new Runnable(){
             @Override
             public void run() {


        if(no_done<questions){//less than total of 8 questions to be done.
            progressBar.setProgress(no_done);
           generateNewAnswer();
            fill_up_answer_choices();
            updateTimer();
            //updateListener();
             }
        else{
            ssf.close();
            //add score achieved here to the person's actual score.

Intent  intent = new Intent(Stage2BActivity.this,CongratulationsActivity.class);
            if(stage3Sequences!=null){

    if(stage3Sequences.isEmpty()){
        sharedPreferences.edit().putLong("SCORE",sharedPreferences.getLong("SCORE",0)+score).apply();

        intent.putExtra("SCORE_OR_TOTAL",fromIntent.getLongExtra("score",0)+score);
        intent.putExtra("DESCRIPTION","ステージ３やった！ええねん！");
        intent.putExtra("CURRENT_SCORE",sharedPreferences.getLong("SCORE",0));

    }else {

        intent = new Intent(Stage2BActivity.this, Stage1BActivity.class);
        intent.putIntegerArrayListExtra("stage3Sequences", stage3Sequences);
        intent.putExtra("score", fromIntent.getLongExtra("score", 0) + score);
        startActivity(intent);
    }
            }else{

            //not doing stage 3:
            sharedPreferences.edit().putLong("SCORE",sharedPreferences.getLong("SCORE",0)+score).apply();


            intent.putExtra("SCORE_OR_TOTAL",Integer.toString(8)+"の質問で"+Long.toString(score)+"できた");
            intent.putExtra("DESCRIPTION","文　ステージやった！");
            intent.putExtra("CURRENT_SCORE",sharedPreferences.getLong("SCORE",0));

        } startActivity(intent);
    }

              getWindow().getDecorView().setBackgroundColor(getResources().getColor(R.color.defaultUnselected));
          }
      };
      Handler handler = new Handler();
      handler.postDelayed(task,500);

    }
    private void question_answered_incorrectly(){


        countDownTimer.cancel();
        vibrator.vibrate(500);
        getWindow().getDecorView().setBackgroundColor(getResources().getColor(R.color.wrong_answer));
        task = new Runnable(){
            @Override
            public void run() {
                streak=0;
                generateNewAnswer();
                fill_up_answer_choices();
                updateTimer();

                getWindow().getDecorView().setBackgroundColor(getResources().getColor(R.color.defaultUnselected));
            }
        };
        Handler handler = new Handler();
        handler.postDelayed(task,1500);
    }


    //methods below are for helping organisation of code .
    @Override
    protected void onStop(){
        super.onStop();
        countDownTimer.cancel();
        Log.e("ON STOP","ON STOP CACLLEd");

    }
    @Override
    protected void onPause(){
        super.onPause();
        countDownTimer.cancel();
        Log.e("ON pause","ON pause CACLLEd");

    }
    private void updateTimer(){
        long timeAllowed=50000;
        if(countDownTimer !=null)
            countDownTimer.cancel();
        time=(long) (timeAllowed*Math.pow(((6-sharedPreferences.getInt("DIFFICULTY",3))/(double)(5*8)),
                (double)streak/(double)8));


        countDownTimer= new CountDownTimer(time,100) {
            @Override
            public void onTick(long millisUntilFinished) {
                timeRemaining.setText(Long.toString(millisUntilFinished/1000) +"."+Long.toString((millisUntilFinished%1000)/100));
            }

            @Override
            public void onFinish() {
                question_answered_incorrectly();
            }
        };
        countDownTimer.start();
    }

    private void generateNewAnswer(){
        indexCount=0;
        currentSuggestion = ssf.getSuggestion();
        sentenceHint.setText(currentSuggestion.toString(Suggestion.MODE_JAP));
        currentSentenceEnding = currentSuggestion.getCharactersForAnsChoices(Suggestion.MODE_KANSAI).toArray(new Character[]{});

        endingHint.setText(currentSuggestion.getSentenceArray(Suggestion.MODE_JAP)[3]+currentSuggestion.getSentenceArray(Suggestion.MODE_JAP)[4]);
        mySentenceString =currentSuggestion.getSentenceArray(Suggestion.MODE_KANSAI)[0]+currentSuggestion.getSentenceArray(Suggestion.MODE_KANSAI)[1]+currentSuggestion.getSentenceArray(Suggestion.MODE_KANSAI)[2] ;

        kansaiAnswerButton.setText(mySentenceString);

    }

    private void fill_up_answer_choices(){

        Random random = new Random();
        ArrayList<Character> answerChoices = new ArrayList<>();
        ArrayList<Character>sentenceArrList = new ArrayList<>();

        //populate the sentence ArrayList. We are using this because the actual currentSentenceEnding array is fixed
        // and cannot have its elements removed and then the array resized.
        for(char c: currentSuggestion.getCharacters(Suggestion.MODE_KANSAI))
            sentenceArrList.add(c);


        char correct = currentSentenceEnding[indexCount];
        //add in the correct choice to the choices.
        answerChoices.add(correct);
      //add in the other choices:
        for( int i =0;i<5;i++)
            answerChoices.add(sentenceArrList.remove(random.nextInt(sentenceArrList.size())));

        //populate the buttons with the possible answer choices.
        b1.setText(answerChoices.remove(random.nextInt(answerChoices.size())).toString());
        b2.setText(answerChoices.remove(random.nextInt(answerChoices.size())).toString());
        b3.setText(answerChoices.remove(random.nextInt(answerChoices.size())).toString());
        b4.setText(answerChoices.remove(random.nextInt(answerChoices.size())).toString());
        b5.setText(answerChoices.remove(random.nextInt(answerChoices.size())).toString());
        b6.setText(answerChoices.remove(random.nextInt(answerChoices.size())).toString());



    }
}
