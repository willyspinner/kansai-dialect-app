package wilson_j.kansaiben_project;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import wilson_j.kansaiben_project.data.data_persistence.AttributeDatabaseHandler;
import wilson_j.kansaiben_project.data.data_persistence.NounDatabaseHandler;
import wilson_j.kansaiben_project.data.structures.Attribute;
import wilson_j.kansaiben_project.data.structures.Noun;
import wilson_j.kansaiben_project.data.structures.Verb;
import wilson_j.kansaiben_project.data.data_persistence.VerbDatabaseHandler;
import wilson_j.kansaiben_project.data.structures.Word;
import wilson_j.kansaiben_project.data.data_persistence.WordDatabaseHandler;

public class AddVocabActivity extends AppCompatActivity {
int mode;
Spinner spinner;
    //mode = 0 is Noun, mode = 1 is verb., 2 is Word
    int verbMode;//1 2 or 3.
    Button choice;
    EditText inputJap;
   EditText inputEng;
    EditText inputKansai;

AlertDialog.Builder dialogBuilder;
    CheckBox c1;
    CheckBox c2;
    CheckBox c3;
    Button addInput;
    Button exit;
    TextView att1;
    TextView att2;
    TextView att3;

    ArrayList<Attribute> selectedAttributes = new ArrayList<>();

    AttributeDatabaseHandler attributeDatabaseHandler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        attributeDatabaseHandler = new AttributeDatabaseHandler(this,false);
        setContentView(R.layout.activity_add_vocab);
        choice = (Button)findViewById(R.id.button_addVocab_choice);
        addInput = (Button) findViewById(R.id.button_addVocab_addVocab);
        exit = (Button)findViewById(R.id.button_addVocab_exit);
        inputJap = (EditText)findViewById(R.id.editText_addVocab_editTextJap);
        inputEng = (EditText)findViewById(R.id.editText_addVocab_ediTextEng);
        inputKansai = (EditText)findViewById(R.id.editText_addVocab_ediTextKansai);
        spinner = (Spinner)findViewById(R.id.spinner_addVocab_attributeSelector);

        c1 = (CheckBox) findViewById(R.id.checkBox_addVocab_1);
        c2 = (CheckBox) findViewById(R.id.checkBox_addVocab_2);
        c3 = (CheckBox) findViewById(R.id.checkBox_addVocab_3);
        att1 = (TextView)findViewById(R.id.textView_addVocab_att1);
        att2 = (TextView)findViewById(R.id.textView_addVocab_att2);
        att3 = (TextView)findViewById(R.id.textView_addVocab_att3);
        final CheckBox[] checkBoxes = {c1,c2,c3};
        final TextView[] attributeViews = {att1,att2,att3};
        View.OnClickListener checkBoxListener =new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckBox selected = (CheckBox)findViewById(v.getId());
                verbMode= Integer.parseInt(selected.getText().toString());
              //  Toast.makeText(AddVocabActivity.this,Integer.toString(verbMode),Toast.LENGTH_LONG).show();
                selected.setSelected(true);
                ArrayList<Integer> notSelected= new ArrayList<Integer>();
                notSelected.add(1);
                notSelected.add(2);
                notSelected.add(3);
                notSelected.remove(verbMode-1);
                for(int i :notSelected)
                    checkBoxes[i-1].setChecked(false);



            }
        };
       c1.setOnClickListener(checkBoxListener);
        c2.setOnClickListener(checkBoxListener);
        c3.setOnClickListener(checkBoxListener);
        mode =1;
        choice.setText("Verb");
        for(CheckBox cb : checkBoxes)cb.setVisibility(View.VISIBLE);
        inputKansai.setVisibility(View.INVISIBLE);
        inputEng.setVisibility(View.INVISIBLE);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddVocabActivity.this, SettingsActivity.class);
                startActivity(intent);
                finish();
            }
        });

        ArrayList<String> adapterArrayLst = attributeDatabaseHandler.getAllAttributeStrings();
        adapterArrayLst.add("新しい追加/ADD NEW");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(AddVocabActivity.this, android.R.layout.simple_spinner_item,adapterArrayLst );
        spinner.setAdapter(dataAdapter);


spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if(selectedAttributes.size()==3){
            Toast.makeText(AddVocabActivity.this,"もう多すぎや",Toast.LENGTH_LONG).show();
            return;
        }

    if("新しい追加/ADD NEW".equals( parent.getItemAtPosition(position))){


        Toast.makeText(AddVocabActivity.this,"addnew",Toast.LENGTH_LONG).show();
        dialogBuilder = new AlertDialog.Builder(AddVocabActivity.this);
          final EditText txtInput = new EditText(AddVocabActivity.this);

        dialogBuilder.setTitle("新しいアットリビュート");
        dialogBuilder.setMessage("新しいアットリビュートを入力してね：");
        dialogBuilder.setView(txtInput);
        dialogBuilder.setPositiveButton("入力", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if(attributeDatabaseHandler.getAllAttributeStrings().contains(txtInput.getText().toString())){
                    Toast.makeText(AddVocabActivity.this,"もうあるよ！",Toast.LENGTH_LONG).show();
                }else{
                    //add the new attribute to the database.
                   attributeDatabaseHandler.addAttribute(txtInput.getText().toString());
                    //create the new attribute.
                    ArrayList<String> adapterArrayLst = attributeDatabaseHandler.getAllAttributeStrings();
                    Attribute toBeAdded = attributeDatabaseHandler.getAllAttributes().get(adapterArrayLst.size()-1);
                    selectedAttributes.add(0,toBeAdded);

                    //update the textviews.
                    for(int i = 0; i< selectedAttributes.size(); i++)
                        attributeViews[i].setText(selectedAttributes.get(i).getAttributeString());

                    //add the additional "add new" choice, and update the spinner object.
                    adapterArrayLst.add("新しい追加/ADD NEW");
                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(AddVocabActivity.this, android.R.layout.simple_spinner_item,adapterArrayLst );
                    spinner.setAdapter(dataAdapter);



            }
        }});
        dialogBuilder.create().show();

    }else{
        Attribute toBeAdded = attributeDatabaseHandler.getAllAttributes().get(position);
        selectedAttributes.add(0,toBeAdded);
        for(int i = 0; i< selectedAttributes.size(); i++)
            attributeViews[i].setText(selectedAttributes.get(i).getAttributeString());






    }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
});

for(TextView t : attributeViews){
    t.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            TextView selected = (TextView)findViewById(v.getId());
            int index=0;
            if(selected.getText().toString().equals(""))return;
            for(;index<attributeViews.length;index++)
                if(selected==attributeViews[index])break;

                selected.setText("");
            //TODO seems to be an error here in the remove function.
            selectedAttributes.remove(index);

        }
    });
}
        //adding listener for when the add input button is pressed:
        addInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //condition below: everything needs to be full before adding it.
                if(inputJap.getText().toString().equals("")||(attributeViews[0].equals("")||attributeViews[1].equals("")||attributeViews[2].equals(""))){
                    Toast.makeText(AddVocabActivity.this,"まだ完了じゃないよ",Toast.LENGTH_LONG).show();

                    return;}
                dialogBuilder = new AlertDialog.Builder(AddVocabActivity.this);


                dialogBuilder.setTitle("アットリビュートを追加？");
                dialogBuilder.setMessage("");
                dialogBuilder.setNegativeButton("いらん", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                   //empty code. This code generated for the sake of having negative button.
                    }
                });



                switch(mode){
                    //TODO Attributes for cases 0 and 1. Get the spinner for this?
                   //TODO Referencing. Attributes array placed on final, but changed later. Is this still ok ?

                    case 0:Log.e("add","added Noun");
                        final  Noun noun= new Noun(inputJap.getText().toString(), selectedAttributes.toArray(new Attribute[3]));


                        dialogBuilder.setPositiveButton("追加", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                NounDatabaseHandler nounDatabaseHandler = new NounDatabaseHandler(AddVocabActivity.this,false);
                                nounDatabaseHandler.addNoun(noun);
                                nounDatabaseHandler.close();
                                Toast.makeText(AddVocabActivity.this,"追加OK",Toast.LENGTH_LONG).show();
                                inputJap.setText("");
                            }
                        }); dialogBuilder.create().show();

                        break;
                    case 1:



Log.e("add","added verb. moving to dialog.");
                        dialogBuilder.setPositiveButton("追加", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                final Verb verb = new Verb(inputJap.getText().toString(),verbMode, selectedAttributes.toArray(new Attribute[3]));
                                VerbDatabaseHandler verbDbHandler = new VerbDatabaseHandler(AddVocabActivity.this,false);
                                verbDbHandler.addVerb(verb);
                                verbDbHandler.close();
                                Toast.makeText(AddVocabActivity.this,"追加OK",Toast.LENGTH_LONG).show();
                                for(CheckBox cb : checkBoxes)cb.setChecked(false);
                                inputJap.setText("");
                            }
                        }); dialogBuilder.create().show();
                       break;

                    case 2:
                       Word word = new Word (inputEng.getText().toString(),inputJap.getText().toString(),inputKansai.getText().toString());
                        WordDatabaseHandler wordDatabaseHandler = new WordDatabaseHandler(AddVocabActivity.this);
                        if(wordDatabaseHandler.getAllWords().contains(word)){Toast.makeText(AddVocabActivity.this,"もうあるよ",Toast.LENGTH_LONG).show();
                        wordDatabaseHandler.close();
                        break;
                        }
                        else{
                        wordDatabaseHandler.addWord(word);
                        wordDatabaseHandler.close();
                       Toast.makeText(AddVocabActivity.this,"追加OK",Toast.LENGTH_LONG).show();
                        inputJap.setText("");
                        inputEng.setText("");
                        inputKansai.setText("");

                        break;}
                }






                }}
        );

        choice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mode=(mode+1)%3;
                //new mode now.
                switch(mode){
                    case 0:choice.setText("Noun");
                    //checkboxes already set invisible by case 2 below. onCreate initialises mode to 1 so this is ok.
                        inputKansai.setVisibility(View.INVISIBLE);
                        inputEng.setVisibility(View.INVISIBLE);
                        for(TextView tv : attributeViews)tv.setVisibility(View.VISIBLE);
                        spinner.setVisibility(View.VISIBLE);
                    break;
                    case 1:choice.setText("Verb");
                        for(CheckBox cb : checkBoxes)
                            cb.setVisibility(View.VISIBLE);
                        //the kansai and eng textboxes are already invisible. So no need to put setVisibility() here
                        //anymore.


                        break;
                    case 2: choice.setText("Kansai vocab");
                        inputKansai.setVisibility(View.VISIBLE);
                        inputEng.setVisibility(View.VISIBLE);
                        for(CheckBox cb : checkBoxes)
                            cb.setVisibility(View.INVISIBLE);
                        spinner.setVisibility(View.INVISIBLE);
                        for(TextView tv : attributeViews)tv.setVisibility(View.INVISIBLE);
               }

            }
        });

    }
}
