package wilson_j.kansaiben_project;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

import wilson_j.kansaiben_project.data.structures.Word;
import wilson_j.kansaiben_project.data.data_persistence.WordDatabaseHandler;

public class Stage1AActivity extends AppCompatActivity {
Button english ;
    Button japanese;
    Button kansai;
    Button exit;
    WordDatabaseHandler db;
    TextView totalTextView;
    TextView counterTextView;
    int counter;
    int total;
    ProgressBar progressBar;
    ArrayList<Word >words;
    String vocabListType;
    //TODO determine vocablistType everytime.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stage1_a);
        english = (Button) findViewById(R.id.button_stage_1a_english);
        japanese = (Button) findViewById(R.id.button_stage_1a_japanese);
        kansai = (Button) findViewById(R.id.button_stage_1a_kansai);
        totalTextView = (TextView)findViewById(R.id.textView_stage_1a_total);
        counterTextView = (TextView) findViewById(R.id.textView_stage_1a_remaining);
        exit = (Button)findViewById(R.id.button_stage_1a_exit);
        progressBar = (ProgressBar)findViewById(R.id.progressBar_stage_1a_progressBar);

        db = new WordDatabaseHandler(this);
        words = db.getAllWords();
        total  =words.size();
        progressBar.setMax(total);
        progressBar.setProgress(0);
        totalTextView.setText(Integer.toString(total));
        counter=0;
exit.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Intent intent = new Intent (Stage1AActivity.this,MainActivity.class);
        startActivity(intent);
    }
});
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!words.isEmpty()){
                    Word currentWord = words.get(0);

                    counter++;
                    progressBar.setProgress(counter);
                    counterTextView.setText(Integer.toString(counter));
                    words.remove(0);
                    english.setText(currentWord.getEng_meaning());
                    japanese.setText(currentWord.getJap_meaning());
                    kansai.setText(currentWord.getKansai_meaning());
                }

                else{

                    Intent intent = new Intent(Stage1AActivity.this,CongratulationsActivity.class);

                    intent.putExtra("DESCRIPTION","Stage 1A ");
                    intent.putExtra("SCORE_OR_TOTAL",Integer.toString(total));
                    intent.putExtra("CURRENT_SCORE",getSharedPreferences("sharedPreferences",MODE_PRIVATE).getLong("SCORE",0));
                    startActivity(intent);


                }

            }
        };

        english.setOnClickListener(listener);
        japanese.setOnClickListener(listener);
        kansai.setOnClickListener(listener);

    }
}
