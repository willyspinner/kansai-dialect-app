package wilson_j.kansaiben_project;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.audiofx.BassBoost;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import wilson_j.kansaiben_project.Helper.DIFFICULTY_VALUES;

public class SettingsActivity extends AppCompatActivity {
SeekBar seekBar;
    TextView textView_difficulty;
    SharedPreferences sharedPreferences;
    Button delete_all;
    Button back;
    Button addVocab;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        seekBar = (SeekBar)findViewById(R.id.seekBar_settings_difficulty);
        textView_difficulty = (TextView)findViewById(R.id.textView_settings_difficulty);
        delete_all = (Button)findViewById(R.id.button_settings_deleteAll);
        back = (Button)findViewById(R.id.button_settings_return);
        addVocab = (Button)findViewById(R.id.button_settings_addVocab);
        sharedPreferences = getSharedPreferences("sharedPreferences",MODE_PRIVATE);
        int difficulty = sharedPreferences.getInt("DIFFICULTY",1);
        textView_difficulty.setText("難しさ："+DIFFICULTY_VALUES.values[difficulty-1]);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SettingsActivity.this,MainActivity.class));
            }
        });

        seekBar.setMax(100);
        seekBar.setProgress((int)((difficulty/(double)5)*(double)100));
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                int new_difficulty = 1+(int)(4*(progress/(double)100));

                sharedPreferences.edit().putInt("DIFFICULTY",new_difficulty).apply();
                textView_difficulty.setText("難しさ："+DIFFICULTY_VALUES.values[new_difficulty-1]);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                seekBar.setBackgroundColor(getResources().getColor(R.color.Selected));
                //TODO is this cosmetic stuff needed?
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                seekBar.setBackgroundColor(getResources().getColor(R.color.defaultUnselected));
            }
        });


        delete_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingsActivity.this,IntroductoryActivity.class);
                startActivity(intent);
                finish();

                //Once the person navigates to the introductory activity, then everything in shared preferences is deleted.
            }
        });
        addVocab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingsActivity.this,AddVocabActivity.class);
                startActivity(intent);

            }
        });
    }
}
