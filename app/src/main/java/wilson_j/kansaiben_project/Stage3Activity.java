package wilson_j.kansaiben_project;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.Random;

public class Stage3Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stage3);

        //This activity does not have a gui. Instead, it leads the user to stages 2 and 1 by an arraylist below:

        Intent intent = new Intent(this,Stage2BActivity.class);
Random random = new Random();
        //this arraylist contains the number of questions to be done in the stages 1 and 2.

        ArrayList<Integer> sequence = new ArrayList<>();
        for(int i =0;i<4;i++){//4 alternating exercises.
            sequence.add(random.nextInt(4-i)+1);


        }

        //put data that is to be carried on to stages 1 and 2. i.e., the array list and the overall score.
                intent.putIntegerArrayListExtra("stage3Sequences",sequence);
        intent.putExtra("score",0);

        startActivity(intent);
    }
}
