package wilson_j.kansaiben_project;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Toast;
import java.io.BufferedReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import wilson_j.kansaiben_project.Helper.DIFFICULTY_VALUES;
import wilson_j.kansaiben_project.data.data_persistence.AttributeDatabaseHandler;
import wilson_j.kansaiben_project.data.data_persistence.NounDatabaseHandler;
import wilson_j.kansaiben_project.data.structures.Attribute;
import wilson_j.kansaiben_project.data.structures.Noun;
import wilson_j.kansaiben_project.data.structures.SentenceSuggestionFactory;
import wilson_j.kansaiben_project.data.structures.Verb;
import wilson_j.kansaiben_project.data.data_persistence.VerbDatabaseHandler;
import wilson_j.kansaiben_project.data.structures.Word;
import wilson_j.kansaiben_project.data.data_persistence.WordDatabaseHandler;

public class IntroductoryActivity extends AppCompatActivity {
    EditText name;
    int difficulty;
    SeekBar difficultySeekBar;

    Button button;
    SharedPreferences sharedPreferences;

    //all the databases to be initialised below:
    WordDatabaseHandler wordDatabaseHandler;
    AttributeDatabaseHandler attributeDatabaseHandler;
    VerbDatabaseHandler verbDbHandler;
    NounDatabaseHandler nounDatabaseHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_introductory);
        name = (EditText)findViewById(R.id.editText_introductory_name);
        difficultySeekBar=(SeekBar)findViewById(R.id.seekBar_introductory_difficulty);
        button = (Button)findViewById(R.id.button_introductory_begin);
        sharedPreferences = getSharedPreferences("sharedPreferences",MODE_PRIVATE);

        sharedPreferences.edit().remove("NAME").remove("DIFFICULTY").remove("SCORE").apply();



        difficultySeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

        difficulty = 1+(int)(4*(progress/(double)100));
        button.setText(DIFFICULTY_VALUES.values[difficulty-1]);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
seekBar.setBackgroundColor(getResources().getColor(R.color.Selected));
    //TODO is this cosmetic stuff needed?
        }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        seekBar.setBackgroundColor(getResources().getColor(R.color.defaultUnselected));
    }
});
        difficultySeekBar.setMax(100);

        difficulty =1;//default.

        //the sentence suggestion factory object below is used to initialise the trigram and relational arrays.


button.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {

        if(name.getText().toString().equals("")){
            //Toast toast = new Toast(IntroductoryActivity.this);
            Toast.makeText(IntroductoryActivity.this,"名前を入力してね",Toast.LENGTH_LONG).show();

        }else{

            //put in all the INITIAL persistent data values of sharedPreferences.
            initDBvalues();
            sharedPreferences.edit().putString("NAME",name.getText().toString()).apply();
            sharedPreferences.edit().putBoolean("FIRSTLOAD",false).apply();
            sharedPreferences.edit().putInt("DIFFICULTY",difficulty).apply();
            sharedPreferences.edit().putLong("SCORE",0).apply();
            sharedPreferences.edit().putInt("CURRENT_STAGE",1).apply();

            //it moved here.
            Intent intent = new Intent(IntroductoryActivity.this,MainActivity.class);
            startActivity(intent);
        }
    }
});


    }

void initDBvalues(){
    if(!sharedPreferences.getBoolean("FIRSTLOAD",true)){
        Log.e("DATABASE popltd","DATABASE alr. POPULATED");

    }else{

        //get all words... for stage 1....
             wordDatabaseHandler = new WordDatabaseHandler(this);

        AssetManager assetManager = this.getAssets();
    try{
    InputStream csvStream = assetManager.open("DefaultWords.csv");

    BufferedReader reader = new BufferedReader(new InputStreamReader(csvStream));
        String line;
        while ((line = reader.readLine()) != null) {
            String[] rowData = line.split(",");
            //initialise all numeric variables of the word to 0.
            Word newWord = new Word(rowData[0],rowData[1],rowData[2],0,0,0.0);
            /*where rowdata[] indexes 0, 1 and 2 link to English, Japanese and Kansai
            translations directly. */
            wordDatabaseHandler.addWord(newWord);
        }

    }catch(Exception e){
        Toast.makeText(this,"CSV WORD can't be read! No text in database-ERROR",
                Toast.LENGTH_LONG).show();
    }


        //initialise default attributes... for stage 2&3....
        Log.e("s","moving on to att.s");
        attributeDatabaseHandler = new AttributeDatabaseHandler(this,true);
        attributeDatabaseHandler.init();
        Log.e("s","atts ok added ");

        try{
            InputStream csvStream = assetManager.open("DefaultAttributes.csv");
            BufferedReader reader = new BufferedReader(new InputStreamReader(csvStream));
            String line;
            while ((line = reader.readLine()) != null) {

               attributeDatabaseHandler.addAttribute(line);
//Log.e("s","adding attributes ok :"+line);
            }

        }catch(Exception e){
            Toast.makeText(this,"CSV ATT. can't be read! No text in database-ERROR",
                    Toast.LENGTH_LONG).show();
        }





        ArrayList<Attribute> attributeArrayList = attributeDatabaseHandler.getAllAttributes();


        //initialise the nouns.
        nounDatabaseHandler = new NounDatabaseHandler(this,true);
        //only this database has an init method invoked as this initialises the trigram.
    nounDatabaseHandler.init();

        try{
            InputStream csvStream = assetManager.open("DefaultNouns.csv");
            BufferedReader reader = new BufferedReader(new InputStreamReader(csvStream));
           // Log.e("noun","open asset noun ok");
            String line;
            //reader.readLine();
            while ((line = reader.readLine()) != null) {

                String[] rowData = line.split(",");
               // Log.e("noun","split noun ok");
                //Log.e("noun rowdata",rowData[0]+"_"+ rowData[1]+"_"+rowData[2]+"_"+rowData[3]);
                Noun newNoun = new Noun(rowData[0],
                        new Attribute[]{
                                attributeArrayList.get(Integer.parseInt(rowData[1])),
                                attributeArrayList.get(Integer.parseInt(rowData[2])),
                                attributeArrayList.get(Integer.parseInt(rowData[3]))});
              //  Log.e("prepared noun ",rowData[0]);
                nounDatabaseHandler.addNoun(newNoun);
              //  Log.e("added noun ",rowData[0]);
            }



        }catch(IOException e){
            Log.e("error","CSV Noun can't be read! No text in database-ERROR");
            Toast.makeText(this,"CSV NOUN can't be read! No text in database-ERROR",
                    Toast.LENGTH_LONG).show();
        }




        //initialise default verbs... for stage 2&3....
         verbDbHandler = new VerbDatabaseHandler(this,true);
//verbDbHandler.init();
        try{

            InputStream csvStream = assetManager.open("DefaultVerbs.csv");
            BufferedReader reader = new BufferedReader(new InputStreamReader(csvStream));

            Log.e("open","open verbcsv ok");

            String line;
            while ((line = reader.readLine()) != null) {
                //Log.e("open","readline ok VERBS ok");
                String[] rowData = line.split(",");
               // Log.e("open","split ok VERBS ok");
               // Log.e("Verb rowdata",rowData[0]+"_"+ rowData[1]+"_"+rowData[2]+"_"+rowData[3]+"_"+rowData[4]);
                Verb newVerb = new Verb(rowData[0],Integer.parseInt(rowData[1]),
                                        new Attribute[]{
                                                attributeArrayList.get(Integer.parseInt(rowData[2])),
                                                attributeArrayList.get(Integer.parseInt(rowData[3])),
                                                attributeArrayList.get(Integer.parseInt(rowData[4]))});
                Log.e("added verb initialised.",rowData[0]);
                verbDbHandler.addVerb(newVerb);

            }

        }catch(Exception e){
            Log.e("error","CSV VERB can't be read! No text in database-ERROR");
            Toast.makeText(this,"CSV VERB can't be read! No text in database-ERROR",
                    Toast.LENGTH_LONG).show();
        }
verbDbHandler.close();



}
}}
