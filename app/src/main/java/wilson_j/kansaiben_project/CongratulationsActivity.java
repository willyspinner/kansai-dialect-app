package wilson_j.kansaiben_project;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class CongratulationsActivity extends AppCompatActivity {
TextView textView_description;
    TextView textView_scoreOrTotal;
    TextView textView_currentScore;
    Button exit;
    SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_congratulations);


        textView_description = (TextView)findViewById(R.id.textView_congrats_description);
        textView_scoreOrTotal = (TextView)findViewById(R.id.textView_congrats_scoreOrTotal);
        textView_currentScore = (TextView)findViewById(R.id.textView_congrats_currentScore);

        exit = (Button)findViewById(R.id.button_congrats_end);
        sharedPreferences = getSharedPreferences("sharedPreferences",MODE_PRIVATE);
        Intent intent = getIntent();
        String description = intent.getStringExtra("DESCRIPTION");
        String scoreOrTotal = intent.getStringExtra("SCORE_OR_TOTAL");
        long currentScore = intent.getLongExtra("CURRENT_SCORE",0);

        //update level based on score..
        if(currentScore>=100&&currentScore<200){
            sharedPreferences.edit().putInt("CURRENT_STAGE",2).apply();
        }else if(currentScore>=200){
            sharedPreferences.edit().putInt("CURRENT_STAGE",3).apply();
        }
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(CongratulationsActivity.this,MainActivity.class);
                startActivity(intent1);

            }
        });
        textView_description.setText(description);
        textView_scoreOrTotal.setText(scoreOrTotal);
        textView_currentScore.setText("今のスコア： "+currentScore);


    }
}
