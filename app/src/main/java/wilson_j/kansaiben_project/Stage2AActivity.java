package wilson_j.kansaiben_project;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

import wilson_j.kansaiben_project.data.structures.SentenceSuggestionFactory;
import wilson_j.kansaiben_project.data.structures.Suggestion;
import wilson_j.kansaiben_project.data.structures.Verb;

public class Stage2AActivity extends AppCompatActivity {
String[][] explanations={{"Negative inflection","In kansai japanese, we use へん in place of ない to conjugate for negative verbs." +
        "   simply replace and it will do!"},
                         {"copulas","関西弁 uses the following copulas: (Kansai= Jap)" +
                                 "〜や＝〜だ, " +
                                 "〜やな＝〜んだね, " +
                                 "〜やで＝〜んだよ, " +
                                 "〜ねん＝〜のだ・のよ, " +
                                 "〜やん＝〜じゃない."+
                         "As you see. it is very simple! "},

                         {"prohibition ","to say　〜てはいけません (prohibition) in kansai, simply replace the はいけません to はアカン. " +
                                 "Kansai uses アカン to denote that something is dangerous, not allowed or undesirable."},
                         {"permission","similar to the prohibition one, we use ~てもええ instead of the standard" +
                                 "japanese 〜てもいい . Simple substitution does the trick."}};

    Button next;
    Button back;
    Button wierdSentenceButton;
    SentenceSuggestionFactory ssf;
    TextView explanation1;
    TextView explanation2;
    TextView exampleSentence;
    TextView indexTextView;
    int currentIndex;
   Suggestion currentSuggestion;
    RatingBar ratingBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stage2_a);
    ssf = new SentenceSuggestionFactory(this);
        next = (Button)findViewById(R.id.button_stage_2a_nextButton);
        back = (Button)findViewById(R.id.button_stage_2a_backButton);
        wierdSentenceButton = (Button)findViewById(R.id.button_stage_2a_wierdButton);
        explanation1= (TextView)findViewById(R.id.textView_stage_2a_currentGrammarPoint);
        explanation2= (TextView)findViewById(R.id.textView_stage_2a_description2);
        exampleSentence= (TextView)findViewById(R.id.textView_stage_2a_exampleUsage);
        indexTextView = (TextView)findViewById(R.id.textView_stage_2a_index);
        ratingBar = (RatingBar)findViewById(R.id.ratingBar_stage_2a_suggestionRating);
        ratingBar.setMax(5);
        ratingBar.setNumStars(5);
        ratingBar.setProgress(5);
        currentIndex=0;
        updateTextViews();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(currentIndex==0){
                Intent intent = new Intent(Stage2AActivity.this,MainActivity.class);
                startActivity(intent);}else{
                    currentIndex--;
                    ssf.setRating(currentSuggestion,(short)ratingBar.getProgress());
                    updateTextViews();
                }
            }
        });
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
               // Toast.makeText(Stage2AActivity.this,"new Rating by getProgress: "+ratingBar.getProgress(),Toast.LENGTH_LONG).show();
            }
        });
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentIndex++;
                ssf.setRating(currentSuggestion,(short)ratingBar.getProgress());
                if(currentIndex==explanations.length){
                     Intent intent = new Intent(Stage2AActivity.this,CongratulationsActivity.class);

                    intent.putExtra("DESCRIPTION","Stage 2A オケ ");
                    intent.putExtra("SCORE_OR_TOTAL",1);
                    intent.putExtra("CURRENT_SCORE",getSharedPreferences("sharedPreferences",MODE_PRIVATE).getLong("SCORE",0));
                    startActivity(intent);
                }else{
                    updateTextViews();

                }
            }
        });
        wierdSentenceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ssf.setRating(currentSuggestion,(short)0);

                updateTextViews();
            }
        });

    }
    @Override
    protected void onPause(){
        super.onPause();
      ssf.close();
    }
    @Override
    protected void onStop(){
        super.onStop();
        ssf.close();
    }

    private void updateTextViews(){

        explanation1.setText(explanations[currentIndex][0]);
        explanation2.setText(explanations[currentIndex][1]);
     currentSuggestion = ssf.getSuggestion();
        String[] sentenceArray= currentSuggestion.getSentenceArray(Suggestion.MODE_KANSAI);
        String text="";
        indexTextView.setText(Integer.toString(currentIndex+1));
        switch(currentIndex){

            case 0://negative inflection
                text=sentenceArray[0]+sentenceArray[1]+sentenceArray[2]+currentSuggestion.getVerb().negativeInflection(Verb.MODE_KANSAI);
                break;
            case 1://copula.
                text=sentenceArray[0]+sentenceArray[1]+sentenceArray[2]+(new Random().nextBoolean()?currentSuggestion.getVerb().negativeInflection(Verb.MODE_KANSAI):currentSuggestion.getVerb().getVerbRoot())+sentenceArray[4];
                break;
            case 2://prohibition
                text= sentenceArray[0]+sentenceArray[1]+sentenceArray[2]+currentSuggestion.getVerb().prohibitionInflection(Verb.MODE_KANSAI);
                break;
            case 3://permission
                text= sentenceArray[0]+sentenceArray[1]+sentenceArray[2]+currentSuggestion.getVerb().permissionInflection(Verb.MODE_KANSAI);
                break;
        }
        exampleSentence.setText(text);


    }
}
