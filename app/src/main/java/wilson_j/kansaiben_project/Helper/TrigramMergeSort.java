package wilson_j.kansaiben_project.Helper;

import android.util.Log;

import java.util.ArrayList;


public class TrigramMergeSort {
    private ArrayList<Integer[]> arrayList;
    private Integer[][] helper;

    private int number;


    public ArrayList<Integer[]> sort(ArrayList<Integer[]> toBeSorted) {
        arrayList = toBeSorted;
        number = toBeSorted.size();
        this.helper = new Integer[number][];
        mergesort(0, number - 1);
        for(Integer[] ii :toBeSorted) Log.e("tag from tms",Double.toString(ii[0].intValue()/(double)ii[1].intValue()));
        return toBeSorted;
    }

    private void mergesort(int low, int high) {
        // check if low is smaller then high, if not then the array is sorted
        if (low < high) {
            // Get the index of the element which is in the middle
            int middle = low + (high - low) / 2;
            // Sort the left side of the array
            mergesort(low, middle);
            // Sort the right side of the array
            mergesort(middle + 1, high);
            // Combine them both
            merge(low, middle, high);
        }
    }

    private void merge(int low, int middle, int high) {

        // Copy both parts into the helper array
        for (int i = low; i <= high; i++) {
            helper[i]= arrayList.get(i);
        }

        int i = low;
        int j = middle + 1;
        int k = low;
        // Copy the smallest values from either the left or the right side back
        // to the original array
        while (i <= middle && j <= high) {

            //the two if statements below are used in order to regard NaN values as 0.0 . (i.e. the lowest).
            double value1 =(helper[i][0].intValue()/(double)(helper[i][1]).intValue());
            double value2= (helper[j][0].intValue()/(double)helper[j][1].intValue());
            value1 = Double.isNaN(value1)?0.0:value1;
            value2 = Double.isNaN(value2)?0.0:value2;
            if (value1 >= value2) {
                arrayList.set(k, helper[i]);

                i++;
            } else {
                arrayList.set(k,helper[j]);
                j++;
            }
            k++;
        }
        // Copy the rest of the left side of the array into the target array
        while (i <= middle) {
            arrayList.set(k,helper[i]);
            k++;
            i++;
        }

    }
}
