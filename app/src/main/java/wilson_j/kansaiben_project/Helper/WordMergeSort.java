package wilson_j.kansaiben_project.Helper;

import wilson_j.kansaiben_project.data.structures.Word;

/**
 * Created by willyspinner on 1/24/17.
 */

public class WordMergeSort {
    private Word[] wordsArr;
    private Word[] helper;

    private int number;


    public Word[] sort(Word[] words) {
        wordsArr = words;
        number = words.length;
        this.helper = new Word[number];
        mergesort(0, number - 1);
        return words;
    }

    private void mergesort(int low, int high) {
        // check if low is smaller then high, if not then the array is sorted
        if (low < high) {
            // Get the index of the element which is in the middle
            int middle = low + (high - low) / 2;
            // Sort the left side of the array
            mergesort(low, middle);
            // Sort the right side of the array
            mergesort(middle + 1, high);
            // Combine them both
            merge(low, middle, high);
        }
    }

    private void merge(int low, int middle, int high) {

        // Copy both parts into the helper array
        for (int i = low; i <= high; i++) {
            helper[i]=wordsArr[i];
        }

        int i = low;
        int j = middle + 1;
        int k = low;
        // Copy the smallest values from either the left or the right side back
        // to the original array
        while (i <= middle && j <= high) {
            if (helper[i].getSuccess_rate() <= helper[j].getSuccess_rate()) {
                wordsArr[k] = helper[i];
                i++;
            } else {
                wordsArr[k] = helper[j];
                j++;
            }
            k++;
        }
        // Copy the rest of the left side of the array into the target array
        while (i <= middle) {
            wordsArr[k] = helper[i];
            k++;
            i++;
        }

    }
}
