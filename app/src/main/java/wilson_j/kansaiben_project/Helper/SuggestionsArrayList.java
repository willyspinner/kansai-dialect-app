package wilson_j.kansaiben_project.Helper;

import java.util.ArrayList;

import wilson_j.kansaiben_project.data.structures.Suggestion;


public class SuggestionsArrayList {
//a sorted arraylist implementation. Used in the SentenceSuggestionFactory object.s
    private ArrayList<Suggestion> arrayList;
    private ArrayList<Double>scores;
    public SuggestionsArrayList(){
    arrayList = new ArrayList<>();
        scores= new ArrayList<>();
    }
    public Suggestion getSuggestion(int i){
        return arrayList.get(i);}
    public int size(){
        return arrayList.size();
    }
    public boolean insertSuggestion(double score, Suggestion suggestion){

        if(arrayList.size()==0){
            arrayList.add(suggestion);
        scores.add(score);return true;
        }
        if(arrayList.size()<4){
            int index=0;
            for(;index<arrayList.size();index++){
                if(scores.get(index)>score)break;
            }
            arrayList.add(index,suggestion);
            scores.add(index,score);

            return true;
        }else {
            //Otherwise, the array is full and one needs to be replaced with this (if it is higher than the lowest score).
         //if it is lower than the lowest value than simply disregard the suggestion. (this is automatic as per arrlist trimmings
            //below. (i.e. remove(0) function)).
            int index=0;
            for(;index<arrayList.size();index++){
                if(scores.get(index)>score)break;
            }
            arrayList.add(index,suggestion);
            scores.add(index,score);
            //below: trim the arraylists sizes down.
            arrayList.remove(0);
            scores.remove(0);
        }
    return true;}

}
