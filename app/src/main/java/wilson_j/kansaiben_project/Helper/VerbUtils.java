package wilson_j.kansaiben_project.Helper;

/**
 * Created by willyspinner on 11/24/16.
 */


public final class VerbUtils {

    private static String INCORRECTINPUT = "Incorrect Input!";

    public static String conjugateAdjNegative(String adj){

        return null;
    }
    public static String conjugateVerbNegative(String verbRoot,int type){
        String ksbEnding="へん";
        switch(type){
            case 1:
                switch(Character.toString(verbRoot.charAt(verbRoot.length()-1))){
                    case "る":return verbRoot.substring(0, verbRoot.length()-1)+"ら"+ksbEnding;
                    case "う":return verbRoot.substring(0, verbRoot.length()-1)+"わ"+ksbEnding;

                    case "つ":return verbRoot.substring(0, verbRoot.length()-1)+"た"+ksbEnding;
                    case "ぶ":return verbRoot.substring(0, verbRoot.length()-1)+"ば"+ksbEnding;
                    case "む":return verbRoot.substring(0, verbRoot.length()-1)+"ま"+ksbEnding;

                    case "ぬ":return verbRoot.substring(0, verbRoot.length()-1)+"な"+ksbEnding;
                    case "く":return verbRoot.substring(0, verbRoot.length()-1)+"か"+ksbEnding;
                    case "ぐ":return verbRoot.substring(0, verbRoot.length()-1)+"が"+ksbEnding;
                    case "す":return verbRoot.substring(0, verbRoot.length()-1)+"さ"+ksbEnding;

                }
                break;
            case 2:
                return verbRoot.substring(0, verbRoot.length()-1)+ksbEnding;




            case 3:
                return verbRoot.substring(0, verbRoot.length()-2)+"せ"+ksbEnding;
            default: return INCORRECTINPUT;
        }
        return null;
    }

    public String conjugateVerbNegativePast(String dictVerb, int type){
        String root=conjugateVerbNegative(dictVerb,type);
        if(root.equals(INCORRECTINPUT))return INCORRECTINPUT;
        return root+"かった";
    }
}
