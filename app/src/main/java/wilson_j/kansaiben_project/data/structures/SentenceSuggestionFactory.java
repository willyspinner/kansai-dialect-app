package wilson_j.kansaiben_project.data.structures;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.util.ArrayList;
import java.util.Random;

import wilson_j.kansaiben_project.Helper.SuggestionsArrayList;
import wilson_j.kansaiben_project.data.data_persistence.NounDatabaseHandler;
import wilson_j.kansaiben_project.data.data_persistence.VerbDatabaseHandler;


public class SentenceSuggestionFactory  {
    private final   String trigramFileName = "DirectTrigram.ser";
    private final   String ovRelationFileName = "ov_relationArray.ser";
    private final   String svRelationFileName = "sv_relationArray.ser";

    private   Random random = new Random();

    private   Context context;
    private DirectTrigram trigram;

    private AttributeRelationArray sv_relationArray;
    private AttributeRelationArray ov_relationArray;


    private   ArrayList<Noun> allNouns;
    private   ArrayList<Verb> allVerbs;
   private NounDatabaseHandler nounDatabaseHandler;
    private VerbDatabaseHandler verbDatabaseHandler;
    public SentenceSuggestionFactory(Context context){
        this.context=context;
        //initialise variables.
         nounDatabaseHandler = new NounDatabaseHandler(context,false);
      verbDatabaseHandler = new VerbDatabaseHandler(context,false);
        allNouns = nounDatabaseHandler.getAllNouns();
        allVerbs = verbDatabaseHandler.getAllVerbs();
Log.e("SSF",allNouns.toString());



        try {
            trigram= (DirectTrigram) loadObject(trigramFileName);

            sv_relationArray = (AttributeRelationArray)loadObject(svRelationFileName);

            ov_relationArray = (AttributeRelationArray)loadObject(ovRelationFileName);

        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(context,"COULD NOT reload trigrams /arrays", Toast.LENGTH_LONG).show();
        }
        }




    public void setRating(Suggestion suggestion, short rating) {
        //sets a rating for a specific suggestion . i.e. s o v.
        trigram.setRating(suggestion.getSubject().getIndex(), suggestion.getObject().getIndex(), suggestion.getVerb().getIndex(), rating);


        //attribute relation for subject:
        for (Attribute a : suggestion.getSubject().getAttributes()) {
            for (Attribute b : suggestion.getVerb().getAttributes()) {
                sv_relationArray.setRating(a.getIndex(), b.getIndex(), rating);
             }
        }
        //attribute relation for object:
        for (Attribute a : suggestion.getObject().getAttributes()) {
            for (Attribute b : suggestion.getVerb().getAttributes()) {
                ov_relationArray.setRating(a.getIndex(), b.getIndex(), rating);
               }
        }
         }

    public void close(){
        //this method is to be invoked when a stage that uses this object is completed.
        try{
        saveObject(trigramFileName,trigram);
        saveObject(svRelationFileName,sv_relationArray);
        saveObject(ovRelationFileName,ov_relationArray);
            }
        catch(IOException ioe){ioe.printStackTrace();}
    }


public Suggestion getSuggestion(){
    //get the best possible subjects. Make it a 3/4  chance of choosing these as the suggestion.
    // why 75% or 3/4? So the AI can learn other subjects as well. Subject should be fixed.
    ArrayList<Noun>possibleSubjects = new ArrayList<>();

    ArrayList<Integer[]>sortedSubjectRatings =  trigram.getSortedSubjectRatings();

    //get the 3 best possible subjects.
   for(int a =0;a<3;a++)possibleSubjects.add(allNouns.get(sortedSubjectRatings.get(a)[2]));

    //for the fourth (last) subject, it is random:
   Noun fourthRandomNoun =allNouns.get(random.nextInt(allNouns.size()));

    //we add a temporary null object for the sake of the arraylist having the same number of elements. This makes it easier
    //for later.

    possibleSubjects.add(fourthRandomNoun);

int subjectAllNounsIndex = random.nextInt(possibleSubjects.size());
    Noun subject = possibleSubjects.get(subjectAllNounsIndex);
       allNouns.remove(subject.getIndex());

    //get objECT. this is randomized.
int objectAllNounsIndex = random.nextInt(allNouns.size());
    Noun object = allNouns.remove(objectAllNounsIndex);

    //restart the allNouns array by calling the database method below.
    allNouns = nounDatabaseHandler.getAllNouns();

    SuggestionsArrayList suggestions = new SuggestionsArrayList();
    //^ the above is used to compare which suggestions fit better. Such suggestions are thrown into an index
    // arraylist, from which a random one will be chosen. It is a modified arraylist.
//this method takes into account all verbs and so is O(n) efficient.

    for(int i = 0; i< allVerbs.size() ; i++) {
        Verb verb = allVerbs.get(i);
        double score=0;
        //^^initialise the score for this specific combination in the loop.
        int directRating=trigram.getRating(subject.getIndex(), object.getIndex(), verb.getIndex());

        if (directRating== -1) {
            //if there is no direct trigram rating, then use relational.
            score = compareRelational(subject,object,verb);


        } else {
            //use trigram.
           score=directRating;
        }
        suggestions.insertSuggestion(score,new Suggestion(subject,object,verb));

        }
    return suggestions.getSuggestion(random.nextInt(suggestions.size()));
}


    private double compareRelational(Noun subject, Noun object, Verb verb){
        int counter=0;
        int score=0;
        //go through both subject and objects and compare them to the verb's attributes. in the relation arrays.

        for(int i=0;i<subject.getAttributes().length;i++){
            for(int j=0;j<verb.getAttributes().length;j++){
                int rating =sv_relationArray.getRating(subject.getAttributes()[i].getIndex(),verb.getAttributes()[j].getIndex());
                if(rating==-1)continue;
         score+=rating;
                counter++;
            }}
        for(int i=0;i<object.getAttributes().length;i++){
            for(int j=0;j<verb.getAttributes().length;j++){
                int rating =ov_relationArray.getRating(object.getAttributes()[i].getIndex(),verb.getAttributes()[j].getIndex());
                if(rating==-1)continue;
                score+=rating;
                counter++;
            }}
        return score/(double)counter;




    }



//methods below are helper methods for saving and loading serialisable files.

    private Object loadObject(String address) throws IOException{
        //helper method to simplify loading objects from serialisable files.



 Object object;
        try{ ObjectInputStream ois = new ObjectInputStream(context.openFileInput(address));
           object = ois.readObject();

            ois.close();
            return object;}
        catch(ClassNotFoundException cnfe){
            return null;
        }



    }
    private void saveObject(String address,Object object) throws IOException{
        //helper method to simplify saving objects to serialisable files.
        FileOutputStream fileOutputStream = context.openFileOutput(address,Context.MODE_PRIVATE);


        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(object);
        objectOutputStream.close();
        fileOutputStream.close();

    }
}
