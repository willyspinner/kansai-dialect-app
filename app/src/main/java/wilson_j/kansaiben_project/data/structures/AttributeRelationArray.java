package wilson_j.kansaiben_project.data.structures;

import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;



public class AttributeRelationArray implements Serializable {
private ArrayList<ArrayList<Integer[]>>attributeRelationArraylist;
    //[0] is the rating. [1] is the number of occurrences.

    public AttributeRelationArray(){

        attributeRelationArraylist=new ArrayList<ArrayList<Integer[]>>();
    }
    public int getRating(int nounAttIndex, int verbAttIndex ){

        return attributeRelationArraylist.get(nounAttIndex).get(verbAttIndex)[0];
    }
        public void setRating(int nounAttIndex, int verbAttIndex ,int rating){
            if( rating<0 || rating>5 )return;


            //1. increment the noOfTimes element by 1:
            attributeRelationArraylist.get(nounAttIndex).get(verbAttIndex)[1]++;
            //if rating is really bad, then make it 0 at all costs. This weighs all above everything.
            if(rating==0){
                attributeRelationArraylist.get(nounAttIndex).get(verbAttIndex)[0]=0;
                return;}

            int newNoOfTimes= attributeRelationArraylist.get(nounAttIndex).get(verbAttIndex)[1];
            //2. calculate new rating and round to nearest whole number.
            int newRating = Math.round(((newNoOfTimes-1)*attributeRelationArraylist.get(nounAttIndex).get(verbAttIndex)[0]+rating)
                            /               (float)newNoOfTimes);
            //3. assign new rating to element at specified indices.
              attributeRelationArraylist.get(nounAttIndex).get(verbAttIndex)[0]=newRating;
       }

        public void addNewAttribute(){
            //resizes the arraylist so that an n by n arraylist is now an n+1 by n+1 arraylist.
            //default value is 5 here. We assume that everything matches. Then the user corrects and alters.

            //add new row:
            for(ArrayList<Integer[]> arrlist:attributeRelationArraylist)
                arrlist.add(new Integer[]{5,0});
               // arrlist.add(new Integer[]{-1,0});

            //add new column , i.e. the column called toBeAdded.
            ArrayList<Integer[]> toBeAdded = new ArrayList<Integer[]>();
            for( int i=0;i<attributeRelationArraylist.size()+1;i++)
                toBeAdded.add(new Integer[]{5,0});
                //toBeAdded.add(new Integer[]{-1,0});
            attributeRelationArraylist.add(toBeAdded);






        }

}
