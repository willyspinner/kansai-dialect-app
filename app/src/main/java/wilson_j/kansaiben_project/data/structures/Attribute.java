package wilson_j.kansaiben_project.data.structures;

/**
 * Created by willyspinner on 2/20/17.
 */

public class Attribute {
    private String attribute;
   private int index;
    public int getIndex(){return index;}
        public String getAttributeString(){return attribute;}

    public Attribute(int index, String attribute){
        this(attribute);
        this.index=index;

    }
    public Attribute(String attribute){
        this.attribute=attribute;
    }
}
