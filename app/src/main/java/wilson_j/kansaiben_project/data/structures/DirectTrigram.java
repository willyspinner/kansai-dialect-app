package wilson_j.kansaiben_project.data.structures;

import android.support.v7.widget.ThemedSpinnerAdapter;
import android.util.Log;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;

import wilson_j.kansaiben_project.Helper.TrigramMergeSort;


public class DirectTrigram implements Serializable {
    //two main data structures:
    private ArrayList<ArrayList<ArrayList<Short>>> trigram;// direct rating for specific s o v.

    private ArrayList<Integer[]>subjectRatings;
    //[0] is rating, [1] is number of times. , [2] is index. Index is used so that when doing sorting
    // in terms of average subject rating,
    // the subject index is still known.
    //to calculate average, simply [0] / [1] (double convert)

    public DirectTrigram(){
        trigram = new ArrayList<ArrayList<ArrayList<Short>>>();

        subjectRatings = new ArrayList<>();
    }

    public ArrayList<Integer[]> getSortedSubjectRatings(){
        ArrayList<Integer[]> sRatingsArrayList = new ArrayList<>(subjectRatings);

        TrigramMergeSort tms = new TrigramMergeSort();


        return tms.sort(sRatingsArrayList);
    }

    public ArrayList<ArrayList<ArrayList<Short>>> getArrayList(){
        return this.trigram;
    }

    public void setRating(int s, int o, int v, short rating){
        if(trigram.get(s).get(o).get(v)==-1){
            subjectRatings.get(s)[0]=subjectRatings.get(s)[0]+(int)rating;


        }else{
            //update.
            subjectRatings.get(s)[0]=subjectRatings.get(s)[0]-(int)trigram.get(s).get(0).get(v)+(int)rating;
        }

        subjectRatings.get(s)[1]++;
        trigram.get(s).get(o).set(v,rating);

    }

    //these are for allocating new space for new S, O or V in the trigram.
    //also initialises all values to -1.

    public void addNewNoun(){
        //recall that the trigram is indexed as follows:
        //trigram.get(S).get(O).get(V) where S O V are the subject, object and verb indices respectively.
    if(trigram.size()==0){
    Log.e("trigram size","size is 0= initing!");
    ArrayList<ArrayList<Short>> arr = new ArrayList<ArrayList<Short>>();
    arr.add(new ArrayList<Short>());
    trigram.add(arr);
    //add to double array:

    subjectRatings.add(new Integer[]{0,0,0});

    }else{
        //add 1  arr<short> to all existing S elements as there is also a new O noun.
        int vLayerSize =trigram.get(0).get(0).size();
        for(ArrayList<ArrayList<Short>> s:trigram){
            ArrayList<Short>newObjectArrList = new ArrayList<Short>();
            for(int i=0;i<vLayerSize;i++)
                newObjectArrList.add((short)-1);
            s.add(newObjectArrList);

            }
           //add 1 arr<arr<short>> to represent the new S noun.
        ArrayList<ArrayList<Short>> newSubjectArray = new  ArrayList<ArrayList<Short>>();
        int oLayerSize = trigram.get(0).size();
        //^ how many Object nouns there are.

        for(int j=0;j<oLayerSize;j++) {
            ArrayList<Short>toBeAdded = new ArrayList<Short>();
            for(int i=0;i<vLayerSize;i++)
                toBeAdded.add((short)-1);
            newSubjectArray.add(toBeAdded);}

        trigram.add(newSubjectArray);

        //add to the double array.

    subjectRatings.add(new Integer[]{0,0,trigram.size()-1});

        }}


    public void addNewVerb(){


        for(ArrayList<ArrayList<Short>> arOuter:trigram){
             for(ArrayList<Short> arInner: arOuter){
                  arInner.add((short)-1);//append new verb trigram value to existing arraylists.
                 //remember that default is -1.
              }
        }
   //     Log.e("trigram",trigram.toString());
     }


    public short getRating(int s, int o, int v){
        return trigram.get(s).get(o).get(v);
    }

}
