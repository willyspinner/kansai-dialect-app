package wilson_j.kansaiben_project.data.structures;

import java.util.ArrayList;

import wilson_j.kansaiben_project.Helper.WordMergeSort;

/**
 * Created by wilson on 1/24/17.
 */

public class PriorityQueue {

    private ArrayList<Word> queueArrlist;
    public PriorityQueue(ArrayList<Word> unsortedArrlist){
        queueArrlist = new ArrayList<Word>();
        Word[] sortedArray = new WordMergeSort().sort(unsortedArrlist.toArray(new Word[unsortedArrlist.size()]));
        for(int i =0; i<sortedArray.length;i++){
         queueArrlist.add(i,sortedArray[i]); }
    }
    public Word dequeue(){
       Word word =  queueArrlist.get(0);
        queueArrlist.remove(0);
        return word;

    }
    public void enqueue(Word word){
        int priority = ((int)Math.ceil((double)(1+queueArrlist.size())/word.getno_temp_wrong()))-1;
         queueArrlist.add(priority,word);
    }
    public ArrayList<Word> getAllWords(){
        return queueArrlist;
    }
    public int size(){
        return queueArrlist.size();
    }
    public boolean isEmpty(){
        return queueArrlist.isEmpty();
    }
}
