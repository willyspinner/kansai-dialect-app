package wilson_j.kansaiben_project.data.structures;

import java.util.ArrayList;
import java.util.Arrays;

public class Verb {

   private String verbRoot;
    private int inflectionType;

    private int index;//index for direct trigram.

    //modes (used for int mode):
   public static int MODE_KANSAI=1;
   public static int MODE_JAP=0;
    private Attribute[] attributes;
    public Verb(int index, String verbRoot, int inflectionType,Attribute[] attributes) {
      this(verbRoot,inflectionType,attributes);
        this.index=index;

    }

    public Verb(String verbRoot, int inflectionType, Attribute[] attributes){
        this.verbRoot = verbRoot;
        this.inflectionType = inflectionType;
        this.attributes=attributes;
    }
    public int getIndex(){
        //ready for trigram use( starts from 0). normally, indexing starts from 1 in sqlite databases, hence the
        //subtraction.
        return index-1;
    }
    public Attribute[] getAttributes(){
        return attributes;
    }

    public String getVerbRoot() {
        return verbRoot;
    }
    public int getInflectionType() {
        return inflectionType;
    }


    public String negativeInflection(int mode){
        String ending;
        if(mode==MODE_KANSAI){
            ending="へん";}
        else{
            ending = "ない";}
        switch(inflectionType){
            case 1:
                switch(Character.toString(verbRoot.charAt(verbRoot.length()-1))){
                    case "う":return verbRoot.substring(0, verbRoot.length()-1)+"わ"+ending;
                    case "る":return verbRoot.substring(0, verbRoot.length()-1)+"ら"+ending;
                    case "つ":return verbRoot.substring(0, verbRoot.length()-1)+"た"+ending;
                    case "ぶ":return verbRoot.substring(0, verbRoot.length()-1)+"ば"+ending;
                    case "む":return verbRoot.substring(0, verbRoot.length()-1)+"ま"+ending;
                    case "ぬ":return verbRoot.substring(0, verbRoot.length()-1)+"な"+ending;
                    case "く":return verbRoot.substring(0, verbRoot.length()-1)+"か"+ending;
                    case "ぐ":return verbRoot.substring(0, verbRoot.length()-1)+"が"+ending;
                    case "す":return verbRoot.substring(0, verbRoot.length()-1)+"さ"+ending;
                    default:return "ERROR!";}
            case 2:
                return verbRoot.substring(0, verbRoot.length()-1)+ending;
            case 3:
                switch(mode){
                    case 0:  return verbRoot.substring(0, verbRoot.length()-2)+"しない";
                    case 1:  return verbRoot.substring(0, verbRoot.length()-2)+"せへん";
                }

            default: return "ERROR";
        }}
    private String teInflection(){
        switch(inflectionType){
            case 1:
                switch(Character.toString(verbRoot.charAt(verbRoot.length()-1))){
                    case "う":return verbRoot.substring(0, verbRoot.length()-1)+"って";
                    case "る":return verbRoot.substring(0, verbRoot.length()-1)+"って";
                    case "つ":return verbRoot.substring(0, verbRoot.length()-1)+"って";
                    case "ぶ":return verbRoot.substring(0, verbRoot.length()-1)+"んで";
                    case "む":return verbRoot.substring(0, verbRoot.length()-1)+"んで";
                    case "ぬ":return verbRoot.substring(0, verbRoot.length()-1)+"んで";
                    case "く":return verbRoot.substring(0, verbRoot.length()-1)+"いて";
                    case "ぐ":return verbRoot.substring(0, verbRoot.length()-1)+"いで";
                    case "す":return verbRoot.substring(0, verbRoot.length()-1)+"して";
                    default:return "ERROR!";}
            case 2:
                return verbRoot.substring(0, verbRoot.length()-1)+"て";
            case 3:
                  return verbRoot.substring(0, verbRoot.length()-2)+"して";
            default: return "ERROR";
                     }
        }

    public String negativePastInflection(int mode){
        String string = negativeInflection(mode);
        if (string==null)return "ERROR";
        return mode == MODE_KANSAI? string+"かった" : string.substring(0, string.length()-1)+"かった";

    }

    public String permissionInflection(int mode){
        return teInflection()+"も"+(mode==MODE_KANSAI?"ええ":"いい");
    }
    public String prohibitionInflection(int mode){
        return teInflection()+"は"+(mode==MODE_KANSAI?"あかん":"いけない");
    }


}

