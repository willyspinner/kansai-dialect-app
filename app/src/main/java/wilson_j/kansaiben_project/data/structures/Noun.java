package wilson_j.kansaiben_project.data.structures;


import java.util.ArrayList;
import java.util.Arrays;

public class Noun {
    private String noun;
    private int index;
    private Attribute[] attributes;// a string array with 3 elements.

    public Attribute[] getAttributes(){
        return attributes;
    }
    public ArrayList<Attribute> getAttributeArrayList(){
        return  (ArrayList<Attribute>) Arrays.asList(attributes);
    }
    @Override
    public String toString() {
        return noun;
    }

    public int getIndex() {
        return index;
    }
    public Noun(String noun, Attribute[] attributes){
        //this constructor is for putting nouns into database.
        this.attributes=attributes;
        this.noun=noun;
    }
    public Noun(String noun,int index,Attribute[] attributes){
        this(noun,attributes);
        this.index=index;

    }

}
