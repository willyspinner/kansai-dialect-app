package wilson_j.kansaiben_project.data.structures;

public class Word {
    //below: variables to be stored in database.
   private String eng_meaning;
    private String jap_meaning;
    private String kansai_meaning;
    private  int no_wrong;
    private int no_right;
    private  double success_rate;
    //below: the only variable that is temporary and isn't stored in the DB.
    private int no_temp_wrong;
    //constructor
    public Word(String eng_meaning, String jap_meaning, String kansai_meaning){
        this.eng_meaning = eng_meaning;
        this.jap_meaning = jap_meaning;
        this.kansai_meaning = kansai_meaning;
        //defaults below.
        this.no_wrong = 0;
        this.no_right =0;
        this.success_rate = 0;
    }
    public Word(String eng_meaning, String jap_meaning, String kansai_meaning,
                int no_wrong, int no_right, double success_rate) {
       this(eng_meaning,jap_meaning,kansai_meaning);
        this.no_wrong = no_wrong;
        this.no_right = no_right;
        this.success_rate = success_rate;
    }

    //getters
    public String getEng_meaning() {
        return eng_meaning;
    }
    public String getJap_meaning() {
        return jap_meaning;
    }
    public String getKansai_meaning() {
        return kansai_meaning;
    }
    public int getno_temp_wrong(){
        return no_temp_wrong;
    }
    public int getNo_wrong(){return no_wrong;}
    public int getNo_right(){return no_right;}
    public double getSuccess_rate(){
    return success_rate;
}
    public void ans_wrong(){
        no_wrong++;
        no_temp_wrong++;
    }
    public void ans_right(){
        no_right++;
        success_rate=(double)no_right/(no_right+no_wrong);
    }
}

