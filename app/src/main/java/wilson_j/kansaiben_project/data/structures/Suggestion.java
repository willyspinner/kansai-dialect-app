package wilson_j.kansaiben_project.data.structures;

import java.util.ArrayList;

import java.util.Random;
public class Suggestion {
    private String questionWord;
    private Noun subject;
    private Noun object;
    private Verb verb;
    private int copula;
  public  static final int MODE_KANSAI=1;
   public static final int MODE_JAP=0;
    //below is an integer that determines the combination as per criterion b.
    private int combinationNumber;
    static final int INFLECTION=0;
    static final int INFLECTIONPAST=1;
    static final int PERMISSION=2;
    static final int PROHIBITION=3;


    //0 : inflection (negative)
    //1 : test inflection (negative past)
  //  2 : test permission
    //3: test prohibition.

   private static  String[] copulasKansai ={"や","やな","やで","か","ねん","やん"};
    private static String[] copulasJap ={"だ","んだね","んだよ","か","んだ","じゃない"};
    private static String[] questionWords ={"なんで","どうやって","いつ","どこで"};


    public Noun getSubject() {
        return subject;
    }

    public Noun getObject() {
        return object;
    }

    public Verb getVerb() {
           return verb;

    }
    public ArrayList<Character> getCharacters(int mode){
        ArrayList<Character> characterArrList= new ArrayList<Character>();
        String[] array = getSentenceArray(mode);
        for(String s: array){
            for(char c: s.toCharArray())characterArrList.add(c);}
        return characterArrList;
        }

    public ArrayList<Character> getCharactersForAnsChoices(int mode){
      ArrayList<Character> characterArrList= new ArrayList<Character>();
        String[] array = getSentenceArray(mode);
        String ansChoice = array[3]+array[4];

            for(char c:ansChoice.toCharArray())
            characterArrList.add(c);

        return characterArrList;
    }
    public String toString(int mode){
        String string = "";
        for(String s: getSentenceArray(mode))
            string =string.concat(s);
        return string;
    }
    public String[] getSentenceArray(int mode){

        String[] array = new String[5];
        array[0]=questionWord;
        array[1]=subject.toString()+"は";
        array[2]=object.toString()+"を";

        switch (combinationNumber){

            case INFLECTION:array[3]=verb.negativeInflection(mode);
                break;
            case INFLECTIONPAST:array[3]= verb.negativePastInflection(mode);break;
            case PERMISSION:array[3]=verb.permissionInflection(mode);break;
            case PROHIBITION:array[3]=verb.prohibitionInflection(mode);break;

        }
        array[4]=getCopula(mode);
return array;
    }

    public String getSentenceString(int mode){
        String a ="";
        String[] array = getSentenceArray(mode);
        for(String s: array)a=a+s;
        return a;
    }

    private String getCopula(int mode) {
        if (copula==-1) return "";
        switch(mode){
            case MODE_JAP:return copulasJap[copula];
            case MODE_KANSAI:    return copulasKansai[copula];
        }
        return "ERROR!";
    }

    public Suggestion(String questionWord, Noun subject, Noun object, Verb verb, int copula) {
        this(subject,object,verb);
        this.questionWord=questionWord;
        this.copula = copula;
    }
    public Suggestion(Noun subject, Noun object, Verb verb){
        this.subject=subject;
        this.object=object;
        this.verb=verb;
        //below is randomisation of other parts of the sentence.
        Random random = new Random();
        copula = random.nextBoolean()?random.nextInt(6):-1;
        combinationNumber = random.nextInt(4);
        questionWord = random.nextBoolean()?questionWords[random.nextInt(questionWords.length)]:"";


    }
}
