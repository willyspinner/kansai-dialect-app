package wilson_j.kansaiben_project.data.data_persistence;

/**
 * Created by willyspinner on 1/23/17.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

import wilson_j.kansaiben_project.data.structures.Word;


public class WordDatabaseHandler extends SQLiteOpenHelper {


    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "WordDatabase";

//table name
    private static final String WORDTABLE = "WordTable";

    // Table Columns names

    private static final String ENG_MEANING = "eng_meaning";
    private static final String JAP_MEANING= "jap_meaning";
    private static final String KANSAI_MEANING = "kansai_meaning";
    private static final String NO_WRONG="no_wrong";
    private static final String NO_RIGHT="no_right";
    private static final String SUCCESS_RATE="success_rate";

    public WordDatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
      //  this.context=context;
    }
    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {


        String CREATE_TABLE = "CREATE TABLE " + WORDTABLE + "("
                    + ENG_MEANING + " TEXT,"
                + JAP_MEANING + " TEXT,"
                + KANSAI_MEANING + " TEXT,"
               +NO_WRONG+ " INTEGER,"
                +NO_RIGHT+ " INTEGER,"
                +SUCCESS_RATE+ " DOUBLE)";

        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+ WORDTABLE);
        onCreate(db);
    }
    public void addWord(Word word){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(ENG_MEANING,word.getEng_meaning());
        values.put(JAP_MEANING,word.getJap_meaning());
        values.put(KANSAI_MEANING,word.getKansai_meaning());
        values.put(NO_WRONG,word.getNo_wrong());
        values.put(NO_RIGHT,word.getNo_right());
        values.put(SUCCESS_RATE,word.getSuccess_rate());

        db.insert(WORDTABLE,null,values);
        db.close();
    }

public void updateWord(Word word){
  //  String kansai = word.getKansai_meaning();
    SQLiteDatabase db = this.getWritableDatabase();
    ContentValues values = new ContentValues();

    values.put(ENG_MEANING,word.getEng_meaning());
    values.put(JAP_MEANING,word.getJap_meaning());
    values.put(KANSAI_MEANING,word.getKansai_meaning());
    values.put(NO_WRONG,word.getNo_wrong());
    values.put(NO_RIGHT,word.getNo_right());
    values.put(SUCCESS_RATE,word.getSuccess_rate());

    db.update(WORDTABLE,values,KANSAI_MEANING+" = ?", new String[]{word.getKansai_meaning()});

}
    public ArrayList<Word> getAllWords(){

        ArrayList<Word> ar = new ArrayList<Word>();
        String selectQuery = "SELECT * FROM "+WORDTABLE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery,null);

        if(cursor.moveToFirst())
        {
            Log.e("cursor movd 2 first.","cursor moved to first.");
            do {
Log.e("a","cursor doing stuff now");
                ar.add(new Word(cursor.getString(0),cursor.getString(1),cursor.getString(2),
                        cursor.getInt(3),cursor.getInt(4),cursor.getDouble(5)));

            }while(cursor.moveToNext());
        }
        return ar;}




}

