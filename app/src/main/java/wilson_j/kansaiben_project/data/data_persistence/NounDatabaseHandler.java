package wilson_j.kansaiben_project.data.data_persistence;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import wilson_j.kansaiben_project.data.structures.Attribute;
import wilson_j.kansaiben_project.data.structures.DirectTrigram;
import wilson_j.kansaiben_project.data.structures.Noun;
import wilson_j.kansaiben_project.data.structures.SentenceSuggestionFactory;


public class NounDatabaseHandler extends SQLiteOpenHelper {


    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "NounsDatabase";

    // Contacts table name
    private static final String NOUNSTABLE = "NounsTable";

    // Contacts Table Columns names
    private static final String INDEX = "_Index";
    private static final String NOUN = "Noun";
    private static final String ATT1="Attribute1Index";
    private static final String ATT2="Attribute2Index";
    private static final String ATT3="Attribute3Index";
   DirectTrigram currentTrigram;
    final String TRIGRAM_ADDRESS = "DirectTrigram.ser";
private boolean firstLoad;
    //attribute database (needed for getting attributes):
    AttributeDatabaseHandler attributeDatabaseHandler;
Context context;
    //below is the constructor for initialisation.
    public NounDatabaseHandler(Context context,boolean firstLoad) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context=context;
        this.firstLoad = firstLoad;
        attributeDatabaseHandler= new AttributeDatabaseHandler(context,firstLoad);
    }
    //below is normal use constructor.

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + NOUNSTABLE + "("
                + NOUN + " TEXT,"+ INDEX + " INTEGER PRIMARY KEY AUTOINCREMENT,"+ATT1+" INTEGER,"+ATT2+" INTEGER,"+ATT3+" INTEGER)";
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+ NOUNSTABLE);
        onCreate(db);
    }
    public void init(){
        currentTrigram = new DirectTrigram();
        saveTrigram();
        //only call this when FIRST TIME LAUNCHING .
    }
    public void addNoun(Noun noun){

      currentTrigram = loadTrigram();
            Log.e("load","Loaded trigram from noundb");

        currentTrigram.addNewNoun();
        saveTrigram();
        //NOTE. Attributes must already have been added.
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(NOUN,noun.toString());
        values.put(ATT1,noun.getAttributes()[0].getIndex());
        values.put(ATT2,noun.getAttributes()[1].getIndex());
        values.put(ATT3,noun.getAttributes()[2].getIndex());
        db.insert(NOUNSTABLE,null,values);
        db.close();

    }


    public ArrayList<Noun>getAllNouns(){

        ArrayList<Noun > ar = new ArrayList<Noun>();
        if(firstLoad)return ar;

        String selectQuery = "SELECT * FROM "+ NOUNSTABLE+" ORDER BY "+INDEX;

        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<Attribute> arrlist=attributeDatabaseHandler.getAllAttributes();
        Cursor cursor = db.rawQuery(selectQuery,null);

        if(cursor.moveToFirst())
        {
            do {

                String noun = cursor.getString(0);
                int index = cursor.getInt(1)-1;
               int att1 = cursor.getInt(2);
                int att2 = cursor.getInt(3);
                int att3 = cursor.getInt(4);



                ar.add(new Noun(noun,index,new Attribute[]{arrlist.get(att1),arrlist.get(att2),arrlist.get(att3)}));

            }while(cursor.moveToNext());
        }
        cursor.close();
        return ar;}

//below are helper methods to save and load the trigrams.
  private DirectTrigram loadTrigram(){
      try{


      ObjectInputStream objectInputStream = new ObjectInputStream(context.getApplicationContext().openFileInput(TRIGRAM_ADDRESS));
      Object object=null;
      try{
          DirectTrigram trigram= (DirectTrigram)objectInputStream.readObject();
          objectInputStream.close();
      return trigram;}
      catch(ClassNotFoundException cnfe){
          return null;
      }



      }catch(Exception e){return null;}
  }
  private void saveTrigram(){
      try{FileOutputStream fileOutputStream = context.getApplicationContext().openFileOutput(TRIGRAM_ADDRESS,Context.MODE_PRIVATE);
      //How to do FOS in android?

      ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
      objectOutputStream.writeObject(currentTrigram);
      objectOutputStream.close();
      fileOutputStream.close();}
      catch(IOException ioe){}

  }
}

