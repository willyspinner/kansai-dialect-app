package wilson_j.kansaiben_project.data.data_persistence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import wilson_j.kansaiben_project.data.structures.Attribute;
import wilson_j.kansaiben_project.data.structures.AttributeRelationArray;
import wilson_j.kansaiben_project.data.structures.DirectTrigram;
import wilson_j.kansaiben_project.data.structures.SentenceSuggestionFactory;

/**
 * Created by willyspinner on 2/18/17.
 */

public class AttributeDatabaseHandler extends SQLiteOpenHelper{

    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "attributeDatabase";

    // Contacts table name
    private static final String ATTRIBUTETABLE = "attributeTable";

    // Contacts Table Columns names
    private static final String INDEX = "indexx";
    private static final String ATTRIBUTE = "attribute";

   private final String SV_ARRAY_ADDRESS="sv_relationArray.ser";
    private final String OV_ARRAY_ADDRESS="ov_relationArray.ser";
    AttributeRelationArray ov_array;
    AttributeRelationArray sv_array;
    Context context;
    boolean firstLoad;
    public AttributeDatabaseHandler(Context context,boolean firstLoad) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
      this.firstLoad= firstLoad;
   this.context=context;
    }
    // Creating Tables

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + ATTRIBUTETABLE + " ("
                + INDEX + " INTEGER PRIMARY KEY , "
                + ATTRIBUTE + " TEXT);";
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+ ATTRIBUTETABLE);
        onCreate(db);
    }
    public void init(){
        //only use on firstLoad.
        sv_array = new AttributeRelationArray();
        ov_array= new AttributeRelationArray();
        saveArrays();
    }
    public void addAttribute(String string){

        //relationArray.
        sv_array = loadArray(SV_ARRAY_ADDRESS);
        ov_array = loadArray(OV_ARRAY_ADDRESS);
        sv_array.addNewAttribute();
        ov_array.addNewAttribute();
        saveArrays();

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(ATTRIBUTE,string);

        db.insert(ATTRIBUTETABLE,null,values);

        db.close();

    }


    public ArrayList<Attribute> getAllAttributes(){

        ArrayList<Attribute > ar = new ArrayList<Attribute>();
        String selectQuery = "SELECT * FROM "+ ATTRIBUTETABLE+" ORDER BY "+INDEX;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery,null);

        if(cursor.moveToFirst())
        {
            do {
                Attribute attribute =new Attribute(cursor.getInt(0)-1,cursor.getString(1));
              //  int index = cursor.getInt(0);

                ar.add(attribute);

            }while(cursor.moveToNext());
        }
        cursor.close();
        return ar;}
//this below is the same as the above, but in string format (i.e. without the index).
public ArrayList<String>getAllAttributeStrings(){
    ArrayList<String>ar = new ArrayList<>();
    for(Attribute a:getAllAttributes())
        ar.add(a.getAttributeString());
    return ar;
}
    private AttributeRelationArray loadArray(String address){
        try{


            ObjectInputStream objectInputStream = new ObjectInputStream(context.getApplicationContext().openFileInput(address));

            try{
                AttributeRelationArray array= (AttributeRelationArray)objectInputStream.readObject();
                objectInputStream.close();
                return array;}
            catch(ClassNotFoundException cnfe){
                return null;
            }



        }catch(Exception e){return null;}
    }
    private void saveArrays(){
        try{FileOutputStream fosOV = context.getApplicationContext().openFileOutput(OV_ARRAY_ADDRESS,Context.MODE_PRIVATE);
            FileOutputStream fosSV = context.getApplicationContext().openFileOutput(SV_ARRAY_ADDRESS,Context.MODE_PRIVATE);
            //How to do FOS in android?

            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fosOV);
            objectOutputStream.writeObject(ov_array);
            objectOutputStream.close();
            fosOV.close();
            ObjectOutputStream objectOutputStream2 = new ObjectOutputStream(fosSV);
            objectOutputStream2.writeObject(sv_array);
            objectOutputStream2.close();
            fosOV.close();}

        catch(IOException ioe){}

    }
}

