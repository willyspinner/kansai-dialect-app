package wilson_j.kansaiben_project.data.data_persistence;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import wilson_j.kansaiben_project.data.structures.Attribute;
import wilson_j.kansaiben_project.data.structures.DirectTrigram;
import wilson_j.kansaiben_project.data.structures.Verb;

public class VerbDatabaseHandler extends SQLiteOpenHelper {

    private AttributeDatabaseHandler attributeDatabaseHandler;
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "VerbsDatabase";

    // Contacts table name
    private static final String VERBSTABLE = "VerbsTable";

    // Contacts Table Columns names
    private static final String INDEX= "VerbIndex";
    private static final String VERBROOT = "VerbRoot";
    private static final String INFLECTIONTYPE= "InflectionType";//type 1 , 2 , 3 for verb inflection.

    private static final String ATT1="Attribute1Index";
    private static final String ATT2="Attribute2Index";
    private static final String ATT3="Attribute3Index";
    private final String TRIGRAM_ADDRESS = "DirectTrigram.ser";
    private Context context;
private DirectTrigram currentTrigram;
    private boolean firstLoad;
    public VerbDatabaseHandler(Context context,boolean firstload) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context=context;
        this.firstLoad =firstload;

        attributeDatabaseHandler= new AttributeDatabaseHandler(context,firstload);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + VERBSTABLE +
                "("+ INDEX + " INTEGER PRIMARY KEY AUTOINCREMENT," + VERBROOT +" TEXT,"+INFLECTIONTYPE+ " INTEGER,"
              //  + ENGLISHMEANING + " TEXT,"
                +ATT1+" INTEGER,"+ATT2+" INTEGER,"+ATT3+" INTEGER)";
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+ VERBSTABLE);
        onCreate(db);
    }

    public void addVerb(Verb verb){





    currentTrigram = loadTrigram(); Log.e("trigram","loaded trigram");

        currentTrigram.addNewVerb();
        saveTrigram();
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
firstLoad =false;
        values.put(VERBROOT,verb.getVerbRoot());
        values.put(INFLECTIONTYPE,verb.getInflectionType());

        values.put(ATT1,verb.getAttributes()[0].getIndex());
        values.put(ATT2,verb.getAttributes()[1].getIndex());
        values.put(ATT3,verb.getAttributes()[2].getIndex());



        db.insert(VERBSTABLE,null,values);
        db.close();

    }


    public ArrayList<Verb>getAllVerbs(){

        ArrayList<Verb> ar = new ArrayList<Verb>();
        if(firstLoad)return ar;
        String selectQuery = "SELECT * FROM "+VERBSTABLE+" ORDER BY "+INDEX;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery,null);
       ArrayList<Attribute> arrlist=attributeDatabaseHandler.getAllAttributes();
        if(cursor.moveToFirst())
        {
            do {
                int idx = cursor.getInt(0)/*-1*/;//sqlite index starts from 0. We want it starting from 0.
                String kanji = cursor.getString(1);
                int inflectionType =cursor.getInt(2);
               // String englishMeaning = cursor.getString(3);
                Attribute[] attributes= new Attribute[]{arrlist.get(cursor.getInt(3)),arrlist.get(cursor.getInt(4)),arrlist.get(cursor.getInt(5))};

                ar.add(new Verb(idx,kanji,inflectionType,attributes));

            }while(cursor.moveToNext());
    }
    cursor.close();
    return ar;}


    private DirectTrigram loadTrigram(){
        try{


            ObjectInputStream objectInputStream = new ObjectInputStream(context.getApplicationContext().openFileInput(TRIGRAM_ADDRESS));
            Object object=null;
            try{
                DirectTrigram trigram= (DirectTrigram)objectInputStream.readObject();
                objectInputStream.close();
                return trigram;}
            catch(ClassNotFoundException cnfe){
                Log.e("trigram not founmd!","trigram not found!");
                return null;
            }



        }catch(Exception e){
            Log.e("trigram not founmd!","returned null. ");return null;}
    }
    private void saveTrigram(){
        try{FileOutputStream fileOutputStream = context.getApplicationContext().openFileOutput(TRIGRAM_ADDRESS,Context.MODE_PRIVATE);
            //How to do FOS in android?

            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(currentTrigram);
            objectOutputStream.close();
            fileOutputStream.close();}
        catch(IOException ioe){}

    }}

