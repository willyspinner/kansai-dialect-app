package wilson_j.kansaiben_project;


import android.content.Intent;
import android.content.SharedPreferences;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.util.Log;
import android.view.DragEvent;
import android.view.View;

import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.ObjectInputStream;

import wilson_j.kansaiben_project.data.structures.DirectTrigram;


public class MainActivity extends AppCompatActivity {
Button toStage1;
    Button toStage2;
    Button toStage3;
    Button toSettings;
    SharedPreferences sharedPreferences;
    TextView greeting;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
     super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toStage1 = (Button) findViewById(R.id.button_main_toStage1);
        toStage2 = (Button) findViewById(R.id.button_main_toStage2);
        toStage3 = (Button) findViewById(R.id.button_main_toStage3);
        toSettings = (Button) findViewById(R.id.button_main_toSettings);
        greeting = (TextView) findViewById(R.id.textView_main_kansaiGreeting);


        sharedPreferences = getSharedPreferences("sharedPreferences",MODE_PRIVATE);


        if(
            sharedPreferences.getBoolean("FIRSTLOAD",true)
                ){

            Intent intent = new Intent(MainActivity.this,IntroductoryActivity.class);
            startActivity(intent);
            return;
        }else{

            greeting.setText("ようこそ、"+sharedPreferences.getString("NAME","ない")+"さん。スコア："+sharedPreferences.getLong("SCORE",0));



            //cosmetics below:
            Log.e("what what",Integer.toString(sharedPreferences.getInt("CURRENT_STAGE",1)));

        toStage1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(MainActivity.this,Stage1AActivity.class);
                    startActivity(intent);
                }
            });
        toStage1.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Intent intent = new Intent(MainActivity.this,Stage1BActivity.class);
                startActivity(intent);
                return true;
            }
        });



        toSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,SettingsActivity.class);
                startActivity(intent);
            }
        });

            toStage2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(sharedPreferences.getInt("CURRENT_STAGE",1)<2){
                        Toast.makeText(MainActivity.this,"まだできへんねん。",Toast.LENGTH_LONG).show();

                    }else{
                        Intent intent = new Intent(MainActivity.this,Stage2AActivity.class);
                        startActivity(intent);

                    }
                }
            });
            toStage2.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if(sharedPreferences.getInt("CURRENT_STAGE",1)<2){
                        Toast.makeText(MainActivity.this,"まだできへんねん。",Toast.LENGTH_LONG).show();
return false;
                    }else{
                        Intent intent = new Intent(MainActivity.this,Stage2BActivity.class);
                        startActivity(intent);

                    }
                    return true;
                }
            });
            toStage3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(MainActivity.this,Stage3Activity.class);

                    if(sharedPreferences.getInt("CURRENT_STAGE",1)<3){
                        Toast.makeText(MainActivity.this,"まだできへんねん。",Toast.LENGTH_LONG).show();

                    }else{
                        startActivity(intent);

                    }
                }
            });
    }
    }



}
