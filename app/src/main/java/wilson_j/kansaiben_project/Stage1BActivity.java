package wilson_j.kansaiben_project;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import wilson_j.kansaiben_project.data.structures.PriorityQueue;
import wilson_j.kansaiben_project.data.structures.Word;
import wilson_j.kansaiben_project.data.data_persistence.WordDatabaseHandler;

import java.util.ArrayList;
import java.util.Random;

public class Stage1BActivity extends AppCompatActivity {
PriorityQueue queue;
ArrayList<Word>permanentWordBank = new ArrayList<>();
    Word currentWord;
    Random random = new Random();
   Button choice1;
    Button choice2;
    Button choice3;
    Button choice4;
    Button hint1;
    Button hint2;
    Button exit;
    TextView timeRemaining;
    Vibrator vibrator;
    TextView scoreTextView;
    SharedPreferences sharedPreferences;
    ProgressBar progressBar;
Bundle savedInstanceState;
    int no_done;
    int total;
    int streak;
    long time;
    int kansaiOrEng;
    long score;
    WordDatabaseHandler dbhandler;
    CountDownTimer countDownTimer;

    View.OnClickListener listener;


    Runnable task;
Intent fromIntent;

    //below isfor stage 3 purposes.
    ArrayList<Integer>stage3Sequences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        this.savedInstanceState=savedInstanceState;
        setContentView(R.layout.activity_stage_1b);
        sharedPreferences= getSharedPreferences("sharedPreferences",MODE_PRIVATE);
        progressBar= (ProgressBar)findViewById(R.id.progressBar_stage_1b_progress);

        timeRemaining = (TextView)findViewById(R.id.textView_stage_1b_timeRemaining);
        scoreTextView = (TextView)findViewById(R.id.textView_stage_1b_score);

        choice1 = (Button)findViewById(R.id.button_stage_1b_ans1);
        choice2 = (Button)findViewById(R.id.button_stage_1b_ans2);
        choice3 = (Button)findViewById(R.id.button_stage_1b_ans3);
        choice4 = (Button)findViewById(R.id.button_stage_1b_ans4);
        hint1 = (Button)findViewById(R.id.button_stage_1b_hint1);
        hint2 = (Button)findViewById(R.id.button_stage_1b_hint2);
        vibrator = (Vibrator)getSystemService(VIBRATOR_SERVICE);
        exit= (Button)findViewById(R.id.button_stage_1b_exit);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Stage1BActivity.this,MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
fromIntent = getIntent();
        kansaiOrEng= random.nextInt(2);
        scoreTextView.setText(Integer.toString(0));
       //if 0, kansai is being tested. If 1, then english is being tested.

        score =0;
         dbhandler = new WordDatabaseHandler(this);

        ArrayList<Word> words= dbhandler.getAllWords();
        if(fromIntent.getIntegerArrayListExtra("stage3Sequences")!=null){

            int maxIndex = fromIntent.getIntegerArrayListExtra("stage3Sequences").remove(0);
            stage3Sequences = fromIntent.getIntegerArrayListExtra("stage3Sequences");
        for(int i=0;i<maxIndex;i++){
            words.remove(words.size()-1);
        }}

        queue = new PriorityQueue(words);
        total= queue.size();
        progressBar.setMax(total);
        progressBar.setProgress(0);
        no_done=0;
        streak=0;
        for(Word word :queue.getAllWords()){
        permanentWordBank.add(word);}

        currentWord=queue.dequeue();
        fill_up_answer_choices();
        updateListener();
        updateTimer();



    }

    @Override
    protected void onStop(){
        super.onStop();
        countDownTimer.cancel();
        Log.e("ON STOP","ON STOP CACLLEd");

    }
    @Override
    protected void onPause(){
        super.onPause();
        countDownTimer.cancel();
        Log.e("ON pause","ON pause CACLLEd");

    }
    public void question_answered_correctly(){
        kansaiOrEng=random.nextInt(2);
        currentWord.ans_right();
        dbhandler.updateWord(currentWord);
        streak++;
        no_done++;
        score += (long) Math.ceil(((Double.parseDouble(timeRemaining.getText().toString())*1000)/(double)time)


                *(10*(streak+1)));
        scoreTextView.setText(Long.toString(score));


        getWindow().getDecorView().setBackgroundColor(getResources().getColor(R.color.right_answer));
        //put the code inside a runnable to allow a delay in the quiz. This is to relax the user from too much
        // intense quizzing.
        task = new Runnable(){
            @Override
            public void run() {

                getWindow().getDecorView().setBackgroundColor(getResources().getColor(R.color.defaultUnselected));
        if(!queue.isEmpty()){
            progressBar.setProgress(no_done);
            currentWord = queue.dequeue();
            fill_up_answer_choices();
            updateTimer();
            updateListener();}
        else{ if(fromIntent.getIntegerArrayListExtra("stage3Sequences")!=null){
            if(stage3Sequences.isEmpty()){
                sharedPreferences.edit().putLong("SCORE",sharedPreferences.getLong("SCORE",0)+score).apply();

                Intent intent = new Intent(Stage1BActivity.this,CongratulationsActivity.class);
                intent.putExtra("score",fromIntent.getLongExtra("score",0)+score);
                intent.putExtra("DESCRIPTION","ステージ３やった！ええねん！");
                intent.putExtra("CURRENT_SCORE",sharedPreferences.getLong("SCORE",0));
startActivity(intent);


            }else{
               Intent intent = new Intent(Stage1BActivity.this,Stage2BActivity.class);
                intent.putIntegerArrayListExtra("stage3Sequences",stage3Sequences);
                intent.putExtra("score",fromIntent.getLongExtra("score",0)+score);
                startActivity(intent);



            }




        }else {


            //add score achieved here to the person's actual score.
            sharedPreferences.edit().putLong("SCORE", sharedPreferences.getLong("SCORE", 0) + score).apply();
            Intent intent = new Intent(Stage1BActivity.this, CongratulationsActivity.class);
            intent.putExtra("SCORE_OR_TOTAL", Integer.toString(total) + "の質問で" + Long.toString(score) + "できた");
            intent.putExtra("DESCRIPTION", "単語ステージやった！");
            intent.putExtra("CURRENT_SCORE", sharedPreferences.getLong("SCORE", 0));
            startActivity(intent);
        }}
            }
        };
        Handler handler = new Handler();
        handler.postDelayed(task,500);
    }
    public void question_answered_incorrectly(){
        kansaiOrEng= random.nextInt(2);
        countDownTimer.cancel();
        vibrator.vibrate(500);
        getWindow().getDecorView().setBackgroundColor(getResources().getColor(R.color.wrong_answer));
        task = new Runnable(){
            @Override
            public void run() {
                currentWord.ans_wrong();
                queue.enqueue(currentWord);
                currentWord = queue.dequeue();
                streak=0;
                fill_up_answer_choices();
                updateTimer();
                updateListener();
                getWindow().getDecorView().setBackgroundColor(getResources().getColor(R.color.defaultUnselected));
            }
        };
        Handler handler = new Handler();
        handler.postDelayed(task,1500);
                                             }



    public void updateTimer(){
        long timeAllowed=20000;
        if(countDownTimer !=null)
        countDownTimer.cancel();
        time=(long) (timeAllowed*Math.pow(((6-sharedPreferences.getInt("DIFFICULTY",3))/(double)(5*total)),
                (double)streak/(double)total));


        countDownTimer= new CountDownTimer(time,100) {
            @Override
            public void onTick(long millisUntilFinished) {
                timeRemaining.setText(Long.toString(millisUntilFinished/1000)+
                        "."+Long.toString((millisUntilFinished%1000)/100));
            }

            @Override
            public void onFinish() {
                question_answered_incorrectly();
            }
        };
        countDownTimer.start();
    }

    public void updateListener(){
        listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               TextView selected = (TextView) findViewById(v.getId());
                switch(kansaiOrEng){
                    case 0:
                        if(selected.getText().toString().equals(currentWord.getKansai_meaning())){
                            question_answered_correctly();return;}
                    case 1:
                        if(selected.getText().toString().equals(currentWord.getEng_meaning())){
                            question_answered_correctly();return;}
                }

                question_answered_incorrectly();

            }
        };
        choice1.setOnClickListener(listener);
        choice2.setOnClickListener(listener);
        choice3.setOnClickListener(listener);
        choice4.setOnClickListener(listener);
    }

    public void fill_up_answer_choices(){
        ArrayList<Word> choices = new ArrayList<Word>();
        choices.add(currentWord);
        //permanent word bank contains all the words in the quiz.
        permanentWordBank.remove(currentWord);
        for(int i =0;i<3;i++){
            int index = random.nextInt(permanentWordBank.size());
            choices.add(permanentWordBank.get(index));
            permanentWordBank.remove(index);}
        for(Word word:choices)
            permanentWordBank.add(word);

        switch(kansaiOrEng){
            case 0:
                choice1.setText(choices.remove(random.nextInt(4)).getKansai_meaning());
                choice2.setText(choices.remove(random.nextInt(3)).getKansai_meaning());
                choice3.setText(choices.remove(random.nextInt(2)).getKansai_meaning());
                choice4.setText(choices.remove(random.nextInt(1)).getKansai_meaning());
                hint1.setText(currentWord.getJap_meaning());
                hint2.setText(currentWord.getEng_meaning());
               break;
            case 1:
                choice1.setText(choices.remove(random.nextInt(4)).getEng_meaning());
                choice2.setText(choices.remove(random.nextInt(3)).getEng_meaning());
                choice3.setText(choices.remove(random.nextInt(2)).getEng_meaning());
                choice4.setText(choices.remove(random.nextInt(1)).getEng_meaning());
                hint1.setText(currentWord.getJap_meaning());
                hint2.setText(currentWord.getKansai_meaning());
                break;
        }

    }
}
